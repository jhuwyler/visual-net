function []=callNet(mpc)
disp('Started rendering mpc file to json')
jsonStr=jsonencode(mpc);
disp('Finished rendering mpc file to json')
comp = computer;
fid = fopen('data/matlabData.json', 'w');
disp('Created matlabData.json file')
if fid == -1, error('Cannot create JSON file'); end
fwrite(fid, 'var mpc =', 'char');
fwrite(fid, jsonStr, 'char');
fclose(fid);
disp('Finished writing data into file')
if ~isempty(strfind(comp,'PCWIN'))
    disp('Opening browser...')
    dos(['index.html'],'-echo');
else
    disp('Opening browser...')
    unix(['open index.html']);
end