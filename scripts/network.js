 //Create array to save selected nodes in
var data;

//graph data
var graph;

//the graph svg
var svg;

//the width of the svg
var width; 

//the height of the svg
var height; 

//the group of graph elements
var svg_group;

//positions of nodes and links
var positions;

// To move single nodes
var MouseDownOnNode = 0;


function drawGraph(json) {
    
    var R, d3cola, enter_nodes, l, links, n, nodes, _i, _j, _len, _len1, _ref, _ref1;

    function getGraphObjFromJson(json){
        /*
        This function creates a graph object that can be used by WebCola to draw the graph.
        */
        var l = interfaceData.bus.length;
        var graph = {}
        var nodes = [];
        var links = []
        
        //Create nodes, the number of buses is obtained from the json
        for(k = 0; k < json.bus.length; k++){
            // k+1 because busnumber starts at 1
  		    var data = {id: k+1};	
  		    nodes[k] = data;   
        }
        
        //Create links, the data for the links is obtained from the json
        for(var k = 0; k < json.branch.length; k++){
            // branch id starts at 0
  		    var data = {id: k, source: json.branch[k].from, target: json.branch[k].to};		
  		    links[k] = data;
        }
        
        graph['nodes'] = nodes;        
        graph['links'] = links;
        
        return graph;
    }

    //Create graph object
 	graph = getGraphObjFromJson(json);
    
    /*
    Match the links to the corresponding nodes.
    The code is adapted from http://bl.ocks.org/nitaku/7891d5fe93150c9faeb7
    */
    _ref = graph.links;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        l = _ref[_i];
        _ref1 = graph.nodes;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            n = _ref1[_j];
            if (l.source === n.id) {
                l.source = n;
            }
            if (l.target === n.id) {
                l.target = n;
            }
        }
    }
    
    // Define the circle radius of the nodes. 
    R = 18; 

    //Select SVG containing topology graph
    svg = d3.select('svg');
    
    //Make a group element for the graph
    container = svg.append('g').attr('id', 'graph_group'); 
    
    //Find width and height for the topology graph, 0.3 of the width is later used for the legend.
    width = 0.7*container.node().getBoundingClientRect().width;
    height = container.node().getBoundingClientRect().height;

  
    //Create links
    links = container.selectAll('.link').data(graph.links, function(d) {
        return d.id;
    });
    
    links.enter().append('line')
        .attr('class', 'link');

    //Create rectangles for the links
    links.each(function(d){
        
        //Create rectangle that will only show underneath the branch when it is critical
        container.append('rect')
            .attr('class', 'bottomLinkRectangle')
            .attr('id', 'bottomLinkRectangle' + parseInt(d.id));
        
        //Create rectangle for the branch
        container.append('rect')
            .attr('class', 'topLinkRectangle')
            .attr('id', 'r' + parseInt(d.id));
        
        //Create line for the first arrow in branch
        container.append('polyline')
                .attr('id', 'arrow1' + parseInt(d.id))
                .attr('class', 'arrow')
                .style("stroke", "black")
                .style('stroke-width', 2)
                .style("fill", "none");
        
        //Create line for the second arrow in branch
        container.append('polyline')
                .attr('id', 'arrow2' + parseInt(d.id))
                .attr('class', 'arrow')
                .style("stroke", "black")
                .style('stroke-width', 2)
                .style("fill", "none");
            
    });
    
    //Create nodes of the topology graph, the nodes correspond to the buses
    nodes = container.selectAll('.node').data(graph.nodes, function(d) {
        return d.id;
    });
    
    enter_nodes = nodes.enter().append('g')
                                    .attr('class', 'node')
                                    .attr('id', function(d) {
        
        return 'g' + d.id;
        
    });
    
    //Create circle for bus that will only show when the bus is critical
    enter_nodes
        .append('circle')
            .attr('class', 'bottomBus')
            .attr('r', R);
    
    //Create circle for bus
    enter_nodes
        .append('circle')
            .attr('class', 'topBus')
            .attr('r', R);

    //Create generators
    for (var i = 0; i < interfaceData.gen.length; i++) {

        generator = d3.select('#g'+ interfaceData.gen[i].genBus); // get the bus where the generator belongs to

        generator.append('line').attr('id', 'generatorLine'+i).attr({x1:0,x2:0,y1:-18,y2:-38}).attr('stroke', 'black').attr('stroke-width','1');
        generator.append('g')
            .attr("class", "generator")
            .attr("id", 'generator'+i)
            .attr('transform','translate(0,-30)')
            .append('rect')
                .attr("class" , "outerGenerator")                
                .attr('width', '16')
                .attr('height', '16')
                .attr('transform','translate(-8,-16)')
                .attr('rx', 4)
                .attr('ry',4);

    }
    
    //Intitilize dragging of nodes
    d3.selectAll('.node')
        .on('mousedown', function(d){
            MouseDownOnNode = 1;
            mouseDownDragNode(d3.event, d);
        })
        .on('mouseup', function(d){
            mouseUpDragNode(d3.event, d);
            MouseDownOnNode = 0;
        });

    //Create zoombox where current zoom level is written
    zoomboxGroup = d3.select('.graphsvg')
                            .append('g')
                                .attr('transform', 'translate(5,5)')
                                .attr('id','zoomboxGroup').on('click', function(d){
                                    zoomReset();
                                });
    zoomboxGroup
        .append('rect')
            .attr({width:50, height:20, id:'zoombox', fill:'white', stroke:'grey', rx:'5', ry:'5'})
    
    zoomboxGroup
        .append('text')
            .attr('id','zoomboxText').text('100%').attr({x:'25', y:'15', 'text-anchor':'middle', fill:'grey'});

    
    //WebCola layout for the topology graph
    graph.nodes.forEach(function(v) {
        
        v.width = 4 * R;
        return v.height = 4 * R;
        
    });
    
    //Calculate min and max value of PMAX in data set. These will be used for boundaries of color scale. 
    calculateMaxAndMinValues ();
    
    d3cola = cola.d3adaptor().size([width, height]).linkDistance(80).avoidOverlaps(true).nodes(graph.nodes).links(graph.links).on('tick', function() {
        /*
        This function updates the positions of the nodes and links of the topology graph.
        */
        
        nodes.attr('transform', function(d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
        
        //Fix nodes to thei positions
        fixNodes();
        
        //Draw rectangles and adjust their thickness
        branchThickness();
        
        //Draw arrow in rectangles
        drawArrow(); 
        
        //Find the starting and end postions of the links
        return links.attr('x1', function(d) {
            return d.source.x;
        }).attr('y1', function(d) {
            return d.source.y;
        }).attr('x2', function(d) {
            return d.target.x;
        }).attr('y2', function(d) {
            return d.target.y;
        });
        
    });
    
    //Create a group for the topology graph
    svg_group = svg.select('#graph_group');
    
    enter_nodes.call(d3cola.drag);
    
    //Indicate number of iterations run by WebCola when creating the topology graph
    d3cola.start(30, 30, 30);
    
 
    //Initialize selection and deselection of components
    selectionAndDeselection();
   
    //Initialize tooltip for the components
    tooltipBus();
    tooltipGenerator();
    tooltipBranch();
    
    //Intialize data display in topology graph
    startUpDataDisplay() ;
    
    // Function to center the network graph horizontally and vertically
    centerGroup();
    
}

d3.selectAll('.node').each(function(d){
    
    d3.select(this).attr("title", d.id);
    
});


//Variables for the drag the whole network graph
var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

function centerGroup(){
    /*
    This function centers the graph horizontally and vertically.
    */
    
    //If the legend is shown, only 70% of the width used. Otherwise entire width can be used.
    if ($('#legendButton').text().trim() == 'Close Legend'){
        
        width = 0.7*parseInt(svg.style('width'));
        
    } else {
        
        width = parseInt(svg.style('width'));
        
    }
    
    //Read tranfrom text of the svg graph and obtain the scale text of the svg group
    var scale_txt = String(svg_group.attr('transform'));

    if(scale_txt.split(' ').length == 2){
        
        scale_txt =' ' + scale_txt.split(' ')[1];
        
    } else {
        
        scale_txt = '';
        
    }
    
    //Update height of the svg
    height = parseInt(svg.style('height'));
    
    //Create text to tranlate graph by
    var trans_txt = 'translate(' + width/2 + ',' + height/2 + ')' + scale_txt;
    
    //Translate svg group
    svg_group.attr('transform', trans_txt);
        
}

//---------------------------------------- Move Network Graph ----------------------------------------//


function mouseDown(event){
    /* 
    This function enables drag and move the whole network graph. When clicking somewhere in the svg the function mouseDown is called to get the position of the curser.
    */
    
    if(MouseDownOnNode === 0){
        
        //Obtain mouse position on mouse down
        pos3 = event.clientX;
        pos4 = event.clientY;
        
        document.onmousemove = mouseMove;
        
        return false;   
        
    }
    
}

function mouseMove(event){
    /*
    This obtains the current posibtion of the curser when the mouse is moved. It is called on mousedown event.
    */
    
    //Optain curser position and the position of the curser to the starting position. 
    pos1 = pos3 - event.clientX;
    pos2 = pos4 - event.clientY;
    pos3 = event.clientX;
    pos4 = event.clientY;
    
    //Obtain graph position
    var str = svg_group.attr('transform').split('(')[1];
    str = str.substr(0, str.length-1);
    var x = str.split(',')[0];
    var y = str.split(',')[1];

    //Obtain scale text
    if(svg_group.attr('transform').split(' ').length > 1){
        
        var scale_txt = ' ' + svg_group.attr('transform').split(' ')[1];
        
    } else {
        
        var scale_txt = '';
        
    }
    
    //Translate the graph group
    var t_x = parseInt(x) - parseInt(pos1);
    var t_y = parseInt(y) - parseInt(pos2);
    var trans_txt = 'translate(' + t_x + ',' +  t_y + ')';
    svg_group.attr('transform', trans_txt + scale_txt);
    
}


function mouseUp(event){
    /*
    This function stops dragging the network graph on mouse up.
    */
    
    document.onmouseup = null;
    document.onmousemove = null;
    
}

//---------------------------------------- Zoom Network Graph ----------------------------------------//

/* 
First add an eventlistener to the svg #graph which calls the function zoom when the zoom wheel or something similar is moved,
*/
document.getElementById("graph").addEventListener("wheel", zoom);

function zoom(event) {
    /*
    This function get values from the zoom wheel and update the scale of the topology graph. 
    */

    //Get tranlate text of the network container
    trans_txt = container.attr('transform');
    
    //The scaleof the network
    var scl;
    
    if(trans_txt.split(' ').length > 1){
        
        scl = trans_txt.split(' ')[1];
        scl = scl.split('(')[1];
        scl = scl.split(')')[0];
        
    } else {
        
        scl = 1;
        
    }

    scl = parseFloat(scl);
    
    //Adapt scale of the network from zoom wheel.
    if(event.deltaY < 0){
        
        scl += 0.01;
        
    }
    if(event.deltaY > 0){
        
        scl -= 0.01;
        
    }
    
    //Apply scale to the network
    var scl_txt = 'scale(' + scl + ')';
    container.attr('transform', trans_txt.split(' ')[0] + ' ' + scl_txt);
    
    //Change text in zoombox
    zoomLevel = Math.round(scl*100)+'%';
    d3.select('#zoomboxText').text(zoomLevel); 
    
}


function zoomReset(){
    /*
    This function resets the zoom to 100%.
    */
    
    //Obtain transform text and remove scale text part
	var trans_txt = container.attr('transform').split(' ')[0];
    
    //Create scale text for the reset
	var scl_txt = 'scale(1)';

    //Reset scale of the networ and text of zoombox
	container.attr('transform', trans_txt + ' ' + scl_txt);
	d3.select('#zoomboxText').text('100%');
    
}


function fixNodes(){
    /*
    This function fixes the position of the nodes, thy can no longer move around freely.
    */
    
    d3.selectAll('.node').each(function(d){
        
        d.fixed = true;
        
    });
    
}


//---------------------------------------- Move Single Nodes ----------------------------------------//

function mouseDownDragNode(event, d){
    /*
    Function to move single nodes and their corresponding generators and links.
    */
    
    //Fix node positions
    fixNodes();
    
    //Get curser coordinates
    pos3 = event.clientX;
    pos4 = event.clientY;
    
    document.onmousemove = mouseMoveDragNode(event, d);
    
}

function mouseUpDragNode(event, d){
    /*
    When the curser is no longer pressed down, stop the movement of the node.
    */
    
    document.onmouseup = null;
    document.onmousemove = null;
    
}

function mouseMoveDragNode(event, d){
    
    //Optain curser position and the position of the curser to the starting position. 
    pos1 = pos3 - event.clientX;
    pos2 = pos4 - event.clientY;
    pos3 = event.clientX;
    pos4 = event.clientY;
    
    //The selected node
    var node = d3.select('#g'+d.id);
   
    //Obtain graph position
    var str = node.attr('transform').split('(')[1];
    str = str.substr(0, str.length-1);
    var x = str.split(',')[0];
    var y = str.split(',')[1];

    if(node.attr('transform').split(' ').length > 1){
        
        scl_txt = ' ' + node.attr('transform').split(' ')[1];
        
    } else {
        
        scl_txt = '';
    
    }
    
    //Translate the graph group
    var t_x = parseInt(x) - parseInt(pos1)
    var t_y = parseInt(y) - parseInt(pos2)
    trans_txt = 'translate(' + t_x + ',' +  t_y + ')';
    node.attr('transform', trans_txt + scl_txt);
    
}

//---------------------------------------- Save Layout ----------------------------------------//

function getPositions(){
    /*
    This function gets the position of the nodes and links and downloads them as a json file for later reference.
    */
    
    //Create string for positions
	var json = '{"nodes":[';
    
    //Save node positions
	d3.selectAll('.node').each(function(d){
		json += '{"x":' + d.x + ', "y":' + d.y + '},';
	});

	json = json.substring(0, json.length - 1);
	json += ']';
    
    //Save link postions
	json += ',"links":['
	d3.selectAll('.link').each(function(d){
		source = d.source;
		target = d.target;
		json += '{"x1":' + source.x + ', "y1":' + source.y + ', "x2":' + target.x + ', "y2":' + target.y +'},';
	});

	json = json.substring(0, json.length - 1);
	json += ']';


	json += '}';
    
    //Obtain date
	var d = new Date();

	var month = d.getMonth();
	if(d.getMonth().toString().length == 1){
		month = '0' + d.getMonth();
	}

	var date = d.getDate();
	if(d.getDate().toString().length == 1){
		date = '0' + d.getDate();
	}

	var hours = d.getHours();
	if(d.getHours().toString().length == 1){
		hours = '0' + d.getHours();
	}

	var minutes = d.getMinutes();
	if(d.getMinutes().toString().length == 1){
		minutes = '0' + d.getMinutes();
	}

	var seconds = d.getSeconds();
	if(d.getSeconds().toString().length == 1){
		seconds = '0' + d.getSeconds();
	}

    //Number of buses
	var NumberOfBuses = d3.selectAll('.node')[0].length;

    //Create string for file name
    var filename = 'NetworkLayout' + NumberOfBuses + 'Bus-' + d.getFullYear() + '-' + month + '-' + date + 'T' + hours + '-' + minutes + '-' + seconds + '.txt';
    
    //save file
	saveData(json, filename);
    
    //Update positions
    positions = json;
    
}


function saveData(data, fileName) {
    /*
    This function saves the postions of the nodes and links to a file.
    */
    
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";

    var json = JSON.stringify(data),
        blob = new Blob([data], {type: "text/plain;charset=utf-8"}),
        url = window.URL.createObjectURL(blob);
    
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);

}


function importPositions(){
    /*
    This function gets JSON file with positions and moves the nodes and links.
    */
    
    //Number of links and nodes in the network
	var numberOfNodes = d3.selectAll('.node')[0].length;
	var numberOfLinks = d3.selectAll('.link')[0].length;
    
    var json = positions;

    try{
        
        json = JSON.parse(json);
        
    } catch(err){ 
        
    	alert('Could not parse text file.')
        
    }
    
    //Number of links and nodes in the imported file
    var numberOfNodesJSON = json.nodes.length;
    var numberOfLinksJSON = json.links.length;

    if(numberOfNodes == numberOfNodesJSON && numberOfLinks == numberOfLinksJSON){

        try{
        
            //Move the nodes the the indicated positions. 
            d3.selectAll('.node').each(function(d){
            
                var nodePos = json.nodes[d.id-1];
                var transl_txt = 'translate(' + nodePos.x + ',' + nodePos.y + ')';
                var node = d3.select('#g' + d.id);
                node.attr('transform', transl_txt);
            
                d.x = nodePos.x;
                d.px = nodePos.x;
                d.y = nodePos.y;
                d.py = nodePos.y;
        
            });
        
            //Move links to designated positions
            d3.selectAll('.link').each(function(d){
            
                var link = d3.select(this);
                var linkPos = json.links[d.id];
                link.attr({x1:linkPos.x1, y1:linkPos.y1, x2:linkPos.x2, y2:linkPos.y2,});
        
            });
        
            //Redraw the rectangles between nodes
            branchThickness();
        
            //Redraw the arrows in the rectangles
            drawArrow();
        
            console.log('Layout loaded!');
    
        } catch(err){
        
            alert('Something went wrong while loading the layout.');
        
        }
        
    } else {
        
        alert('Incorrect number of buses or links.');
        
    }

}   


function loadLayout(){
    /*
    Open file selection dialog when load layout is pressed.
    */

    document.getElementById('fileInput').click();
    
}



window.onload = function() {
    /*
    Get node positions from json textfile and save it to global variable positions.
    */

    var fileInput = document.getElementById('fileInput');

    fileInput.addEventListener('change', function(e) {

        var file = fileInput.files[0];
        var textType = /text.*/;

        if (file.type.match(textType)) {
        
            var reader = new FileReader();

            reader.onload = function(e) {
            
                positions = reader.result;
                
                //Adjust positions of nodes
                importPositions();
        
            }

            reader.readAsText(file);    
    
        } else {
        
            console.log("File not supported!");
    
        }
        
    });
    
}
