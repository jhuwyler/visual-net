
function selectionAndDeselection(){
    /*
    This function select and deselects the buses, generators and branches in the network. 
    */
    
    //Select and deselect buses
    d3.selectAll('.topBus').on('dblclick', function(d){

        if (!d3.select(this).classed('selected') ){
            
            //Make the stroke width of newly selected the bus thicker and class it as selected. 
            d3.select(this).classed('selected', true)
            d3.select(this).style('stroke-width', '2px');
            
        }else{
            
            //Make stroke width normal and class as not selected. 
            d3.select(this).classed('selected', false);
            d3.select(this).style('stroke-width', '1px');
            
        }
        
        //Clear the array 
        selected.elements.bus=[];
        
        //Push selected bus on array.
        d3.selectAll('.topBus').each(function (d, i) {
            
            if(d3.select(this).classed('selected')){
                
                selected.elements.bus.push(d.id)
                
            }
           
        });
        //update the charts
        updateCharts();
        
    });

    //Select and deselect gen
    d3.selectAll('.generator').on('dblclick', function(d){

        if (!d3.select(this).classed('selected') ){
            
            //Make the stroe width of newly selected the generator thicker and class it as selected. 
            d3.select(this).classed('selected', true)
            d3.select(this).style('stroke-width', '2');
            
            //Call the function that fills the generator, this is done to adujust the stroke width. 
            //innerSizeGeneratorRelative ();
            
        }else{
            
            //Make stroke width normal and class as not selected. 
            d3.select(this).classed('selected', false);
            d3.select(this).style('stroke-width', '1');
            
            //Call the function that fills the generator, this is done to adujust the stroke width. 
            //innerSizeGeneratorRelative ();
            
        }
        
        //Clear the array
        selected.elements.gen=[];
        
        //Push selected generator on array
        d3.selectAll('.generator').each(function (d, i) {
            
            if(d3.select(this).classed('selected')){  
                
                selected.elements.gen.push(i+1)
    
            }
           
        });
        //update the charts
        updateCharts();
    });


    //Select and deselect branches
    d3.selectAll('.topLinkRectangle').on('dblclick', function(d){
        
        if (!d3.select(this).classed('selected') ){
            
            //Class selected branches as selected
            d3.select(this).classed('selected', true)
            
            //Make outline of selected branches thicker
            d3.select(this).style('stroke-width', '2');
            
        }else{
            
            //No longer class deselected nodes as selected
            d3.select(this).classed('selected', false);
            
            //Change stroke width back to normal width 
            d3.select(this).style('stroke-width', '1');
        }
    
        //Clear the Array
        selected.elements.branch=[];
        
        //Push selected branches on array
        d3.selectAll('.topLinkRectangle').each(function (d, i) {
            
            if(d3.select(this).classed('selected')){
                
                selected.elements.branch.push(i+1)
                
            }
           
        });
        //update the charts with the new elements
        updateCharts();  
    });

}
