// JavaScript Document for the Startup and Data processing

//check if matlab file was imported if so import the data
if( typeof mpc != 'undefined'){
    try{
    
        importData(mpc, function(){
            $('#startup').hide('slow');
            numberOfTimesteps=interfaceData.bus[0].features[0].data.length;
            $("#selectableMenu").show('slow');
            $("#Slider").show('slow');
            $("#Canvas").show('slow');
            $('#graph').html("<svg class='graphsvg' style='cursor:move'></svg>");
            createSelectable(interfaceData);
            createSlider();
            drawGraph(interfaceData);
            d3.select('#navbarDropdown').attr('class', 'btn btn-light dropdown-toggle ');
            d3.select('#saveButtonLayout').attr('class', 'btn btn-light ');
            d3.select('#loadLayout').attr('class', 'btn btn-light ');
            d3.select('#saveButton').attr('class', 'btn btn-light ');
            d3.select('#legendButton').attr('class', 'btn btn-primary ');
            d3.select('#timegraphbutton').attr('class', 'btn btn-primary ');
        });
    }
    catch(e){
        alert("The Matlab file could not be imported. Maybe your dropped a json file which struct is not named 'mpc'");
        $('#startup').hide('slow');
        $("#import").show('slow');
        console.log(e);
    }
}
else{
    $("#startUpProgressbar").html('No Matlab data detected!');
    $('#startup').hide('slow');
    $("#import").show('slow');
    
}
// function to load javascript files on demand
function loadJs(url, cb) {
  var script = document.createElement('script');
  script.setAttribute('src', url);
  script.setAttribute('type', 'text/javascript');

  var loaded = false;
  var loadFunction = function () {
    if (loaded) return;
    loaded = true;
    cb();
  };
  script.onload = loadFunction;
  script.onreadystatechange = loadFunction;
  document.getElementsByTagName("head")[0].appendChild(script);
};

// onClick function from the examples which loads the corresponding example
function loadExample(busnumber){
    var filename = 'data/bus'+busnumber+'.json';
    var struct;
    $("#import").hide('fast');
    $('#startup').show('fast');
    $("#startUpProgressbar").html('Loading '+busnumber+' Bus Example File...');
    loadJs(filename, function(){
        $("#startUpProgressbar").html('Loaded '+busnumber+' Bus Example File!');

        // get the struct to import the data
        if (busnumber==30){
            struct=bus30;
        }
        if (busnumber==118){
            struct=bus118;
        }
        if (busnumber==300){
            struct=bus300;
        }
        if (busnumber=='30noLimits'){
            struct=bus30noLimits;
        }
        try{
            importData(struct, function(){
                $('#startup').hide('slow');
                numberOfTimesteps=interfaceData.bus[0].features[0].data.length;
                $("#selectableMenu").show('slow');
                $("#Slider").show('slow');
                $("#Canvas").show('slow');
                $('#graph').html("<svg class='graphsvg' style='cursor:move'></svg>");
                createSelectable(interfaceData);
                createSlider();
                drawGraph(interfaceData);
                d3.select('#navbarDropdown').attr('class', 'btn btn-light dropdown-toggle ');
                d3.select('#saveButtonLayout').attr('class', 'btn btn-light ');
                d3.select('#loadLayout').attr('class', 'btn btn-light ');
                d3.select('#saveButton').attr('class', 'btn btn-light ');
                d3.select('#legendButton').attr('class', 'btn btn-primary ');
                d3.select('#timegraphbutton').attr('class', 'btn btn-primary ');
                
                
            });
        }
        catch(e){
            alert("The Example file could not be imported");
            $("#import").show('fast');
            $('#startup').hide('fast');
            console.log(e);
        }
    });
}


function handleDragOver(evt) {
   		evt.stopPropagation();
    	evt.preventDefault();
    	evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  	}

var json;
// Setup the dnd listeners.
var dropZone = document.getElementById('drop_zone');
dropZone.addEventListener('dragover', handleDragOver, false);
dropZone.addEventListener('drop', handleFileSelect, false);

function handleFileSelect(evt) {

		evt.stopPropagation();
   		evt.preventDefault();

   		var files = evt.dataTransfer.files;

		// files is a FileList of File objects. List some properties.
		var output = [];
		for (var i = 0, f; f = files[i]; i++) {
			var reader = new FileReader();

			// Closure to capture the file information.
			reader.onload = (function (theFile) {
				return function (e) {
					 $('#startup').show('slow');
                    $("#startUpProgressbar").html("Loading Drag and Drop File...");
				    json = JSON.parse(e.target.result);
                    try {
                       
						 importData(json, function(){
                            progressLabel.text("Loaded Drag and Drop File!");
                            $('#startup').hide('slow');
                            $("#import").hide('slow');
                            numberOfTimesteps=interfaceData.bus[0].features[0].data.length;
                            $("#selectableMenu").show('slow');
                            $("#Slider").show('slow');
                            $("#Canvas").show('slow');
                            $('#graph').html("<svg class='graphsvg' style='cursor:move'></svg>");
                            createSelectable(interfaceData);
                            createSlider();
                            drawGraph(interfaceData);
                            d3.select('#navbarDropdown').attr('class', 'btn btn-light dropdown-toggle ');
                            d3.select('#saveButtonLayout').attr('class', 'btn btn-light ');
                            d3.select('#loadLayout').attr('class', 'btn btn-light ');
                            d3.select('#saveButton').attr('class', 'btn btn-light ');
                            d3.select('#legendButton').attr('class', 'btn btn-primary ');
                            d3.select('#timegraphbutton').attr('class', 'btn btn-primary ');
                        });

					} catch (ex) {
                        console.log(ex);
						alert("Could not parse dropped file");
					}
				}
			})(f);
			reader.readAsText(f);
		}
}


