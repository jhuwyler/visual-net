//The width of the svg containing the legend. 
var widthLegend;

//The height of the svg containing the legend. 
var heightLegend;

//The number of bus in the data set. 
var numberOfBus; 

//The minimum VMIN of all the bus.
var minVMIN; 

//The maximum VMIN of all the bus. 
var maxVMIN; 

//The minimum VMAX of all the bus.
var minVMAX;

//The maximum VMAX of all the bus. 
var maxVMAX;

//The minimum real power demand of all bus.
var minPD; 

//The maxmimum real power demand of all bus. 
var maxPD; 

//The minimum reactive power demand of all bus.
var minQD; 

//The maximum reactive power demand of all bus.
var maxQD; 

//The minimum apparent power demand of all bus. 
var minSD; 

//The maximum apparent power demand of all bus.
var maxSD;

//The minimal possible radius of the bus. 
var minRadiusBus = 14; 

//The maximal possible radius of the bus. 
var maxRadiusBus = 24; 

//The number of generator in the data set. 
var numberOfGenerator; 

//The maximal PMAX value in data set.
var maxPMAX;

//The minimal PMAX value in data set. 
var minPMAX;

//The maximal PMAX value in data set.
var maxQMAX;

//The minimal PMAX value in data set. 
var minQMAX;

//The maximal PMAX value in data set.
var maxQMIN;

//The minimal PMAX value in data set. 
var minQMIN;

//The minimum width and height of the generator. 
var minWidthGenerator = 18; 

//the maximum width and height of the generator. 
var maxWidthGenerator = 24; 

//The number of branches in the data set.
var numberOfBranches; 

//The maximal A rating of all the branches.
var maxRATE_A;

//The minimal A rating of the branches excluding zero. Zero is excluded, as it implies that the rating in unlimited.
var minRATE_A;

//The minimal thickness of the branches.
var minThicknessBranch = 6;

//The maximum thickness of the branches.
var maxThicknessBranch = 12; 

//This bool keeps track of whether any branch RATE_A is set to zero.
var ratingUnlimited;

//The selected generator feacture to display in networks
var generatorFeature  = 'real power';

//The text of the generator feature for legend
var generatorText = 'real';

//The width of the svg for the network
var widthSvg; 

//The height of the svg for the network
var heightSvg; 


function drawLegendRect (){
    
    //Calculate width and height of svg
    widthSvg = svg.node().getBoundingClientRect().width;
    heightSvg = svg.node().getBoundingClientRect().height;
    
    //Remove rectangle if still present 
    d3.select('#legendRectangle').remove();
    
    //Create group for legend
    svg.append('g').attr('id','legendGroup');
    
    //Append rectangle on which all the legend elements will be placed
    d3.select('#legendGroup')
        .append('rect')
            .attr('id', 'legendRectangle')
            .attr('fill', 'white')
            .attr('width', 0.3*widthSvg)
            .attr('height', heightSvg)
            .attr('y',0)
            .attr('x', 0.7*widthSvg);
    }


function colorLegend () {
    /*
    This function creates a continious color legend. For the fill colors of the bus, generators and branches. The fill colors of the bus goes from the VMIN to VMAX, and those of the branches correspond to the the percentage of RATE_A used.
    */
    
    //Select the legend groupr and white rectangle.
    var legendGroup = d3.select('#legendGroup');
    var legendRectangle = d3.select('#legendRectangle');
    
    
    //Remove a previously defined legend if it is still present. This way they don't overlay. 
    d3.select('#colorLegendBus').remove();
    d3.select('#colorLegendGeneratorBranch').remove();
    d3.select('#colorRect').remove();
    d3.select('#colorText').remove();
    
  
    //Update width and height of the svg for the legend.
    widthLegend = 0.3*widthSvg;
    heightLegend = heightSvg;

    //This float corresponds to the y coordinate of the title for the color legend.
    var colorTextY = 4 / 5 * (heightLegend - 40) + 20 ;
    
    //Create title for the color legend.
    legendGroup.append('text')
        .attr('id', 'colorText')
        .text('Bus and branch color by:')
        .attr('text-anchor', 'left')
        .attr("font-size", "12")
        .attr('font-family', 'sans-serif')
        .attr('y',colorTextY)
        .attr('dx', 0.7*widthSvg);
    
    //Define continuous color gardient. 
    var colorGradient = legendGroup.append('defs').append('svg:linearGradient')
                            .attr('id', "relativeGradient")
                            .attr("x1", "0%")
                            .attr("y1", "100%")
                            .attr("x2", "100%")
                            .attr("y2", "100%")
                            .attr("spreadMethod", "pad");

    //Define colors for the color gradient. Color are defined 5 times along the color gradient.
    colorGradient.append('stop')
        .attr("offset", "0%")
        .attr("stop-color", '#3f5ea9')
        .attr("stop-opacity", 1);
    
    colorGradient.append('stop')
        .attr("offset", "25%")
        .attr("stop-color", '#a3d1e5')
        .attr("stop-opacity", 1); 
    
    colorGradient.append('stop')
        .attr("offset", "50%")
        .attr("stop-color", '#f9f8c2')
        .attr("stop-opacity", 1);
    
    colorGradient.append('stop')
        .attr("offset", "75%")
        .attr("stop-color", '#fa9d5b')
        .attr("stop-opacity", 1);  
    
    colorGradient.append('stop')
        .attr("offset", "100%")
        .attr("stop-color", '#c31e28')
        .attr("stop-opacity", 1);  
    
    //The width of the rectangle with the contiuous color legend.
    var colorRectHeight = 35;
    
    //The y coordinate of the rectangle with the continuous color legend.
    var colorRectY = colorTextY + 50;
    
    //Create rectangle with the fill being the previously defined color gradient. 
    legendGroup.append('rect')
        .attr('id' , 'colorRect')
        .attr('width' , 2 * widthLegend / 3)
        .attr('height' , colorRectHeight)
        .style('fill' , 'url(#relativeGradient)')
        .attr('x', 0.7*widthSvg+widthLegend / 6)
        .attr('y', colorRectY )
        .style('stroke' , 'black');
    
    //Calculate minimal and maximal VMAX and VMIN values. 
    calculateVMINAndVMAX();
    
    //Define y Axis for bus
    var xBus = d3.scale.linear().range([0 , 2 * widthLegend / 3]).domain([minVMIN, maxVMAX]);    
    var xAxisBus = d3.svg.axis().scale(xBus).orient('top').ticks(5);
    
    //Create y Axis and place it on the right side of the previously created rectangle. 
    legendGroup.append('g')
        .attr('id', 'colorLegendBus')
        .attr('class', 'x axis')
        .attr('transform' , 'translate(' + (0.7*widthSvg+widthLegend / 6)+ ',' + (colorRectY) +')')
        .call(xAxisBus)
        .append('text')
            .attr('y' , -30)
            .attr('dx' , widthLegend / 3)
            .style('text-anchor' , 'middle')
            .text('voltage magnitude bus (p.u.)');
    
    //Define y Axis for generator and branches, the ticks are displayed as percentanges.
    var xGeneratorBranch = d3.scale.linear().range([0, 2 * widthLegend / 3]).domain([0, 1]);
    var formatPercent = d3.format(".0%");
    var xAxisGeneratorBranch = d3.svg.axis().scale(xGeneratorBranch).orient('bottom').tickFormat(formatPercent).ticks(5);
    
    //Create y Axis for generator and branch and place it right next to the previously defined rectangle on the other side as legend for bus. 
    legendGroup.append('g')
        .attr('id', 'colorLegendGeneratorBranch')
        .attr('class', 'x axis')
        .attr("transform", "translate(" + (0.7*widthSvg +widthLegend / 6)+ "," + (colorRectY + colorRectHeight) + ")")
        .call(xAxisGeneratorBranch)
        .append('text')
            .attr('y', 35)
            .attr("dx", widthLegend / 3)
            .style('text-anchor', 'middle')
            .text('percentage of RATE_A');
    
}


function calculateVMINAndVMAX () {
    /*
    This function calculates minVMIN, maxVMIN, minVMAX and maxVMAX of the bus in the network. 
    */

    
    //Calculate the number of bus in data set. 
    numberOfBus = interfaceData.bus.length;
    
    //Find the minimum and maximum VMIN for the bus data.     
    minVMIN = interfaceData.bus[0].vmin;
    maxVMIN = interfaceData.bus[0].vmin;
    
    for(var i = 1; i < numberOfBus; i++) {
        
        minVMIN = Math.min(minVMIN, interfaceData.bus[i].vmin);
        maxVMIN = Math.max(maxVMIN, interfaceData.bus[i].vmin); 
        
    }
    
    //Find the minimum and maximum VMAX for the bus. 
    minVMAX = interfaceData.bus[0].vmax;
    maxVMAX = interfaceData.bus[0].vmax;   
    
    for(var i = 1; i< numberOfBus; i++){
        
        minVMAX = Math.min(minVMAX, interfaceData.bus[i].vmax);
        maxVMAX = Math.max(maxVMAX, interfaceData.bus[i].vmax);  
        
    }
    
}


function calculatePDAndQD () {
    /*
    This function calculates minPD, maxPD, minQG, maxQG, minSD and maxSD of the bus in the network. 
    */

    
    //Calculate the number of bus in data set. 
    numberOfBus = interfaceData.bus.length;
    
    //Find the minimum and maximum PD for the bus data.     
    minPD = d3.min(interfaceData.bus[0].features[0].data);
    maxPD = d3.min(interfaceData.bus[0].features[0].data);
    
    for(var i = 1; i < numberOfBus; i++) {
        
        minPD = Math.min(minPD, d3.min(interfaceData.bus[i].features[0].data));
        maxPD = Math.max(maxPD, d3.max(interfaceData.bus[i].features[0].data)); 
        
    }
    
    //Find the minimum and maximum QD for the bus data.     
    minQD = d3.min(interfaceData.bus[0].features[1].data);
    maxQD = d3.min(interfaceData.bus[0].features[1].data);
    
    for(var i = 1; i < numberOfBus; i++) {
        
        minQD = Math.min(minQD, d3.min(interfaceData.bus[i].features[1].data));
        maxQD = Math.max(maxQD, d3.max(interfaceData.bus[i].features[1].data)); 
        
    }
    
    //Find the minimum and maximum SD for the bus data.   
    minSD = Math.sqrt(Math.pow(minPD, 2)+Math.pow(minQD, 2)); 
    maxSD = Math.sqrt(Math.pow(maxPD, 2)+Math.pow(maxQD, 2)); 
    
}


function busLegend () {
    /*
    This function creates a legend for the meaning of the sizes of the bus. 
    */
    
    //Select the legend groupr and white rectangle.
    var legendGroup = d3.select('#legendGroup');
    var legendRectangle = d3.select('#legendRectangle');
   
    //Remove a previously defined legend elements if they are still present. 
    d3.selectAll('.busLegend').remove();
  
    //Update width and height of the rectangle for the legend.
    widthLegend = 0.3*widthSvg;
    heightLegend = heightSvg;
    
    //The y coordinate of the title of the legend for the bus sizes.
    var textY = 20; 
    
    //The y coordinate of the center of the bus.
    var busCenterY = textY + 40; 
    
    //The y coordinate of the text that indicates the meaning of the sizes of the bus.
    var busTextY = busCenterY + maxRadiusBus + 15;
    
    //Create title for the bus size legend.
    legendGroup.append('text')
        .attr('id', 'busText')
        .attr('class', 'busLegend')
        .text('Bus radius by apparent power demand:')
        .attr('text-anchor', 'left')
        .attr("font-size", "12")
        .attr('font-family', 'sans-serif')
        .attr('y',textY)
        .attr('x', 0.7*widthSvg);    
    
    /*
    Check if bus size constant. In the case that the bus size is constant is suffices to draw one circle corresponding to the size of all bus in the network
    */
    if (minRadiusBus == maxRadiusBus){
        
        //Append circle with radius corresponding to the bus radius.
        legendGroup.append('circle')   
            .attr('id', 'midBus')
            .attr('class', 'busLegend')
            .attr('r', minRadiusBus)
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('cx', widthLegend/2+0.7*widthSvg)
            .attr('cy', busCenterY);
    
        //Append label to legend circle.
        legendGroup.append('text')
            .attr('id', 'midBusText')
            .attr('class', 'busLegend')
            .text( minSD+ ' (MVA)')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', busTextY)
            .attr('x',widthLegend/2+0.7*widthSvg)
        
    } else {
        /* 
        We draw 3 circles for the legend if the bus size varies. The circles correspond to the minimal size, the medium size and the maximal size.
        */
        
        //Append circle with radius corresponding to the minimum bus radius
        legendGroup.append('circle')   
            .attr('id', 'minBus')
            .attr('class', 'busLegend')
            .attr('r', minRadiusBus)
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('cx', widthLegend/6+0.7*widthSvg)
            .attr('cy', busCenterY);
        
        //Append legend to minimum circle
        legendGroup.append('text')
            .attr('id', 'minBusText')
            .attr('class', 'busLegend')
            .text( round(minSD,2) + ' (MVA)')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', busTextY)
            .attr('x',widthLegend/6+0.7*widthSvg)
        
        //Append circle with radius corresponding to the medium bus radius
        legendGroup.append('circle')   
            .attr('id', 'midBus')
            .attr('class', 'busLegend')
            .attr('r', (minRadiusBus+maxRadiusBus)/2)
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('cx', widthLegend/2+0.7*widthSvg)
            .attr('cy', busCenterY);
        
        //Append legend to medium circle
        legendGroup.append('text')
            .attr('id', 'midBusText')
            .attr('class', 'busLegend')
            .text(round(((minSD+maxSD)/2),2)+ ' (MVA)')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', busTextY)
            .attr('x',widthLegend/2+0.7*widthSvg)
        
        //Append circle with radius corresponding to the maximum bus radius
        legendGroup.append('circle')   
            .attr('id', 'maxBus')
            .attr('class', 'busLegend')
            .attr('r', maxRadiusBus)
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('cx', 5*widthLegend/6+0.7*widthSvg)
            .attr('cy', busCenterY);
    
        //Append legend to maximum circle
        legendGroup.append('text')
            .attr('id', 'maxBusText')
            .attr('class', 'busLegend')
            .text(round(maxSD,2)+ ' (MVA)')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', busTextY)
            .attr('x',5*widthLegend/6+0.7*widthSvg)     
        
    }
    
}


function colorBusRelative () {
    /*
    This function colors in the bus in accordance to their voltage magnitude. Further, it tracks, whether the conditions specified by VMIN and VMAX are met. In case they are not met, the fill becomes a diagonal hash pattern.
    */
    
    //Set minimal and maximal allowed value for voltage magnitude for all bus.
    var VMIN = minVMIN;
    var VMAX = maxVMAX;   
        
    //Calculate difference between VMIN and VMAX. 
    var deltaV = VMAX-VMIN;
    
    //Set scale for color of bus. 
    var colorScaleBus = d3.scale.linear().domain([VMIN, VMIN + 0.25*deltaV, VMIN  + 0.5*deltaV, minVMIN + 0.75*deltaV, VMAX]).range(['#3f5ea9','#a3d1e5','#f9f8c2','#fa9d5b','#c31e28']);
    
    //Array of integer containg VM for all the bus at current time. 
    var VM = new Array(numberOfBus);
    
    //Array of boolean. If VM is critically low for node i, element i-1 is marked as true. 
    var criticallyLowBus = new Array(numberOfBus); 
    
    //Array of boolean. If VM is critically high for node i, element i-1 is marked as true. 
    var criticallyHighBus = new Array(numberOfBus); 
    
    //Change color of bus.
    d3.selectAll('.topBus').each(function(d) {
        
        //Integer keeping track of VM of current bus. 
        var tempVM;
        
        //Calculate current time value. 
        currentTime=Math.round(slider.value());
        
        //Find VM of bus. 
        tempVM = interfaceData.bus[d.id-1].features[2].data[currentTime];
        
        //Check whether VM falls short of VMIN or exceeds VMAX.  
        if (tempVM < VMIN ) {
            
            //Set tempVM to VMIN in order to not fall short of VMIN on color scale.  
            tempVM = VMIN;
            
            //Set critically high indicator of bus to true. 
            criticallyLowBus[d.id-1] = true;
            criticallyHighBus[d.id-1] = false;
            
        } else if (tempVM > VMAX) {
            
            //Set tempVM to VMAX in order to not exceed VMAX on c
            tempVM = VMAX;
            
            //Set critically high indicator of bus to true. 
            criticallyHighBus[d.id-1] = true;
            criticallyLowBus[d.id-1] = false;
            
           
        } else {
            
            criticallyLowBus[d.id-1] = false;
            criticallyHighBus[d.id-1] = false;
            
        }
        
        //Update VM array with tempVM of current bus.  
        VM[d.id-1] = tempVM;
    
    });
    
    //Define diagonal hash pattern for when VM is critically high
    var patternHigh = svg.append('defs')
                            .append('pattern')
                                .attr('id' , 'hashHigh4_4')
                                .attr('width', 8 )
                                .attr('height' , 8)
                                .attr('patternUnits' , 'userSpaceOnUse')
                                .attr('patternTransform','rotate(60)')
                            .append("rect")
                                .attr('width',4)
                                .attr('height',8)
                                .attr('transform','translate(0,0)')
                                .attr('fill','#c31e28' );
    
    //Define diagonal hash pattern for when VM is critically low
    var patternLow = svg.append('defs')
                            .append('pattern')
                                .attr('id' , 'hashLow4_4')
                                .attr('width', 8 )
                                .attr('height' , 8)
                                .attr('patternUnits' , 'userSpaceOnUse')
                                .attr('patternTransform','rotate(60)')
                            .append("rect")
                                .attr('width',4)
                                .attr('height',8)
                                .attr('transform','translate(0,0)')
                                .attr('fill','#3f5ea9' );
    
    
    //Color in the bus
    d3.selectAll('.topBus').each(function (d) {
        
       if (criticallyLowBus[d.id-1] == false && criticallyHighBus[d.id-1] ==false) {
           
           //Color in bus with color calculated from color scale
           d3.select(this).style('fill', function(d) {
                
                return colorScaleBus(VM[d.id-1])
                
            });
           
        } else if (criticallyLowBus[d.id-1] == true){
            
            //Fill bus with diagonal hash pattern 
            d3.select(this).style('fill', 'url(#hashLow4_4)');
            
        } else if (criticallyHighBus[d.id-1] == true){
            
            //Fill bus with diagonal hash pattern 
            d3.select(this).style('fill', 'url(#hashHigh4_4)');
            
        }            
  
    });
    
}


function adjustStartingPointOfGeneratorLine () {
    /*
    This function adjust the starting point of the generator and the line. This starting point has to be adjusted as the size of the bus is varied.
    */
    
    //Calculate the number of generator in the data set
    numberOfGenerator =interfaceData.gen.length;
    
    
    for (var i = 0; i < numberOfGenerator; i++) {
        
        //Select group in which bus, line to generator and the generator are contained.
        var currentGroup = d3.select('#g'+ interfaceData.gen[i].genBus)
        
        //Select bus to which the generator is attached.
        var currentBus =currentGroup.selectAll('.topBus');
        
        //Get radius of the bus in question.
        var busRadius = currentBus.attr('r');
        
        //Select the generator group containing  both generator rectangles.
        var currentGenerator = currentGroup.selectAll('.generator')
        
        //Translate the rectangles
        currentGenerator.attr('transform','translate('+0+','+(-busRadius-15)+')');
        
        //Select line conntecting bus and generator
        var currentLine =d3.select('#generatorLine'+ i);
        
        //Translate the generator line in correspondance with the bus radius
        currentLine
            .attr('y1',-busRadius)
            .attr('y2',-busRadius-20);
        
    }
    
}
 

function busRadius () {
    /*
    This function changes the size of the bus in accordance to VMAX of the nodes.
    */
    
    //This float keeps track of the value of the real power demand (PD) of the current bus
    var tempPD; 
    
    //This float keeps track of the value of the reactive power demand (QD) of the current bus
    var tempQD; 
    
    //This float keeps track of the value of the apparent power demand (SD) of the current bus
    var tempSD;
    
    //Only change radius is minSD and maxSD are not identical. Otherwise radius can be kept the same.
    if (minSD != maxSD) {
        
        /*
        Define a scale for the radius of the bus. The minimum QMAX corresponds to a radius of minRadiusBus, and the max to a radius of maxRadiusBus.
        */
        var radiusScaleBus = d3.scale.linear().domain([minSD,maxSD]).range([minRadiusBus,maxRadiusBus]);
        
        //Get current time in network.
        var currentTime = Math.round(slider.value());
        
        //Array of float to keep track of the current radius of the bus
        var currentRadius = new Array(numberOfBus);
        
        //Change radius of the bus for the top radius
        d3.selectAll('.topBus')
            .attr('r', function(d) {
                
                //Calculate current values
                tempPD = interfaceData.bus[d.id-1].features[0].data[currentTime];
                tempQD = interfaceData.bus[d.id-1].features[1].data[currentTime];
                tempSD = Math.sqrt(Math.pow(tempPD, 2)+Math.pow(tempQD, 2));
                
                //Calculate, save and change radius
                currentRadius[d.id-1] =radiusScaleBus(tempSD);                
                return radiusScaleBus(tempSD);
        
            });
        
        //Change radius of the bus for the bottom radius. 0.5 is subtracted to account for stroke width.
        d3.selectAll('.bottomBus')
            .attr('r', function(d) {
            
                return (currentRadius[d.id-1] -0.5);
        
            });
        
    } else {
            
        //If minSD and maxSD are identical for all bus, the min and max radius is set to the same value.
        minRadiusBus =18; 
        maxRadiusBus =18;
            
    }
    
}


function calculatePMAX () {
    /*
    This function calculates the lower and upper values of PMAX in the data set.
    */
    
    //Calculate the number of generator
    numberOfGenerator = interfaceData.gen.length;
    
    //Find the minimum and maximum QMAX for the generator. QMAX will determine the width and height of the generator.
    maxPMAX = d3.max(interfaceData.gen[0].features[3].data);
    minPMAX = d3.min(interfaceData.gen[0].features[3].data);
    
    for(var i = 1; i< numberOfGenerator; i++){
        
        maxPMAX = Math.max(maxPMAX, d3.max(interfaceData.gen[i].features[3].data));
        minPMAX = Math.min(minPMAX,d3.min(interfaceData.gen[i].features[3].data));
        
    }
    
}


function calculateQMINAndQMAX () {
    /*
    This function calculates the lower and upper values of QMIN an QMAX in the data set.
    */
    
    //Calculate the number of generator
    numberOfGenerator = interfaceData.gen.length;
    
    /*
    Find the minimum and maximum QMAX for the generator. QMAX will determine the width and height of the generator. It will also correspond to the max fill level.
    */
    minQMAX = interfaceData.gen[0].qmax;
    maxQMAX  = interfaceData.gen[0].qmax;
    
    for(var i = 1; i< numberOfGenerator; i++){
        
        minQMAX = Math.min(minQMAX,interfaceData.gen[i].qmax);
        maxQMAX = Math.max(maxQMAX, interfaceData.gen[i].qmax);
        
    }
    
    /*
    Find the minimum and maximum QMIN for the generator. QMIN corresponds to the max fill level.
    */
    minQMIN = interfaceData.gen[0].qmin;
    maxQMIN  = interfaceData.gen[0].qmin;
    
    for(var i = 1; i< numberOfGenerator; i++){
        
        minQMIN = Math.min(minQMIN,interfaceData.gen[i].qmin);
        maxQMIN = Math.max(maxQMIN, interfaceData.gen[i].qmin);
        
    }
    
}


function generatorLegend () {
    /*
    This funciton creates a legend for the size of the generator.
    */
    
    //The minimum value for the generator size label, it will either correspond to minPMAX or minQMAX.
    var minValue;
    
    //The minimum value for the generator size label, it will either correspond to minPMAX or minQMAX.
    var maxValue;
    
    //The unit for the generator size label.
    var unit;
    
    if  (generatorFeature=='real power') {
        
        //Set min and max values to those of PMAX
        minValue = minPMAX;
        maxValue = maxPMAX;
        unit ='MW';
        
    } else if (generatorFeature=='reactive power') {
        
        //Set min and max values to those of QMAX
        minValue = minQMAX;
        maxValue = maxQMAX;
        unit ='MWAr';
        
    }
    
    //Select the legend group and white rectangle.
    var legendGroup = d3.select('#legendGroup');
    var legendRectangle = d3.select('#legendRectangle');
   
    //Remove a previously defined elements of the legend if they are still present.
    d3.selectAll('.generatorSizeLegend').remove(); 
  
    //Update width and height of the rectangle for the legend.
    widthLegend = 0.3*widthSvg;
    heightLegend = heightSvg;
    
    //The y coordinate of the header for the generator size legend.
    var textY = 2 * (heightLegend-40) / 5 + 30;
    
    //The y coordinate of the generators. This will have to be adjusted with the actual generator width.
    var generatorY = textY + 40;
    
    //The y coordinate of the text labels for the generator.
    var generatorTextY = generatorY +15+ maxWidthGenerator/2;
    
    //Create a title for the bus size legend.
    legendGroup.append('text')
        .attr('id' , 'generatorText')
        .attr('class', 'generatorSizeLegend')
        .text('Generator size by maximum '+ generatorText+' power output:')
        .attr('text-anchor' , 'left')
        .attr('font-size' , '12')
        .attr('font-family' , 'sans-serif')
        .attr('y' , textY)
        .attr('x', 0.7*widthSvg);
    
    
    //Check if the generator size is constant throughout the network.
    if (minWidthGenerator == maxWidthGenerator){
        /* 
        If the size of all generators is constant it is sufficient to draw on rectangle with th corresponding size and label it with PMAX.
        */
        
        //Append rectangle with width and height corresponding to the width and height of the generator.
        legendGroup.append('rect')   
            .attr('id' , 'midGenerator')
            .attr('class', 'generatorSizeLegend')
            .attr('width' , minWidthGenerator)
            .attr('height' , minWidthGenerator )
            .attr('rx' , 3)
            .attr('ry' ,3)            
            .attr('fill' , '#ffffff')
            .attr('stroke' , '#000000')
            .attr('x' , widthLegend/2-minWidthGenerator/2+0.7*widthSvg)
            .attr('y' , generatorY-minWidthGenerator/2);
    
        //Append label for the generator to legend.
        legendGroup.append('text')
            .attr('id', 'midGeneratorText')
            .attr('class', 'generatorSizeLegend')
            .text( round(minValue,2)+ ' ('+unit+')')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY)
            .attr('x',widthLegend/2+0.7*widthSvg)
        
    } else {
        /*
        If the generator size is not identical for all generators, we draw three rectangels with the sizes of the smallest, middle and maximum generator. Each is labeled with the the corresponding PMAX.
        */
        
        //Append rectangle with the dimensions corresponding to the minimum generator dimensions.
        legendGroup.append('rect')   
            .attr('id', 'minGenerator')
            .attr('class', 'generatorSizeLegend')
            .attr('width',minWidthGenerator)
            .attr('height',minWidthGenerator)
            .attr('rx', 3)
            .attr('ry',3)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/6-minWidthGenerator/2+0.7*widthSvg)
            .attr('y', generatorY-minWidthGenerator/2);
        
        //Append label to minimum sized generator.
        legendGroup.append('text')
            .attr('id', 'minGeneratorText')
            .attr('class', 'generatorSizeLegend')
            .text( round(minValue,2)+ ' ('+unit+')')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY)
            .attr('x',widthLegend/6+0.7*widthSvg)
        
        //Append rectangle with the dimensions corresponding to the medium generator dimensions.
        legendGroup.append('rect')   
            .attr('id', 'midGenerator')
            .attr('class', 'generatorSizeLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('rx', 3)
            .attr('ry',3)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/2-(maxWidthGenerator+minWidthGenerator)/4+0.7*widthSvg)
            .attr('y', generatorY - (maxWidthGenerator+minWidthGenerator)/4);
        
        //Append label to medium sized generator. 
        legendGroup.append('text')
            .attr('id', 'midGeneratorText')
            .attr('class', 'generatorSizeLegend')
            .text( round((minValue+maxValue)/2,2) + ' ('+unit+')')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY)
            .attr('x',widthLegend/2+0.7*widthSvg)
        
        //Append rectangle with the dimenstions corresponding to the maxium dimension of the generator.
        legendGroup.append('rect')   
            .attr('id', 'maxGenerator')
            .attr('class', 'generatorSizeLegend')
            .attr('width',maxWidthGenerator )
            .attr('height', maxWidthGenerator )
            .attr('rx', 3)
            .attr('ry',3)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', 5*widthLegend/6-maxWidthGenerator/2+0.7*widthSvg)
            .attr('y', generatorY-maxWidthGenerator/2);
    
        //Append label for the maximum generator. 
        legendGroup.append('text')
            .attr('id', 'maxGeneratorText')
            .attr('class', 'generatorSizeLegend')
            .text( round(maxValue,2)+ ' ('+unit+')')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY)
            .attr('x',5*widthLegend/6+0.7*widthSvg)
        
    }
    
}


function generatorFillLegend () {
    /*
    This funciton creates a legend for the fill and color of generator.
    */
    
    //Select the legend group and white rectangle.
    var legendGroup = d3.select('#legendGroup');
    var legendRectangle = d3.select('#legendRectangle');
   
    //Remove a previously defined elements of the legend if they are still present.
    d3.selectAll('.generatorFillLegend').remove(); 
  
    //Update width and height of the rectangle for the legend.
    widthLegend = 0.3*widthSvg;
    heightLegend = heightSvg;
    
    //The y coordinate of the header for the generator size legend.
    var textY = 3 * (heightLegend-40) / 5 + 15;
    
    //Create a title for the bus size legend.
    legendGroup.append('text')
        .attr('id' , 'generatorFillText')
        .attr('class', 'generatorFillLegend')
        .text('Generator fill by capacity utilization:')
        .attr('text-anchor' , 'left')
        .attr('font-size' , '12')
        .attr('font-family' , 'sans-serif')
        .attr('y' , textY)
        .attr('x', 0.7*widthSvg);
    
    if  (generatorFeature=='real power') {
        //Create legend for real power of generator displayed
        
        //The y coordinate of the generators. This will have to be adjusted with the actual generator width.
        var generatorY = textY + 40;
    
        //The y coordinate of the text labels for the generator.
        var generatorTextY = generatorY +15+ maxWidthGenerator/2;
        
        //Append rectangle with the dimensions corresponding to no fill, mid size generators are used.
        legendGroup.append('rect')   
            .attr('id', 'noFillGenerator')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator)/2)
            .attr('height' , (minWidthGenerator+maxWidthGenerator)/2)
            .attr('rx',(minWidthGenerator+maxWidthGenerator)/8)
            .attr('ry',(minWidthGenerator+maxWidthGenerator)/8)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/6-(minWidthGenerator+maxWidthGenerator)/4 +0.7*widthSvg)
            .attr('y', generatorY-(minWidthGenerator+maxWidthGenerator)/4);
        
        //Append label to the no fill generator.
        legendGroup.append('text')
            .attr('id', 'noFillGeneratorText')
            .attr('class', 'generatorFillLegend')
            .text('0%')
            .attr('font-size', '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY)
            .attr('x',widthLegend/6+0.7*widthSvg)
        
        //Append rectangle corresponding to mid fill, mid size generator are used.
        legendGroup.append('rect')   
            .attr('id', 'midOuterFillGenerator')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator)/8)
            .attr('ry',(minWidthGenerator+maxWidthGenerator)/8)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/2-(maxWidthGenerator+minWidthGenerator)/4+0.7*widthSvg)
            .attr('y', generatorY - (maxWidthGenerator+minWidthGenerator)/4);
    
        //Append fill rectangle for mid fill.
        legendGroup.append('rect')   
            .attr('id', 'midInnerFillGenerator')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator + maxWidthGenerator - 1) / 4 )
            .attr('height', (minWidthGenerator + maxWidthGenerator - 1) / 4 )
            .attr('rx', (minWidthGenerator + maxWidthGenerator - 1) / 16)
            .attr('ry',(minWidthGenerator + maxWidthGenerator - 1) / 16)            
            .attr('fill', '#7e7e7e')
            .attr('x', widthLegend / 2 - (minWidthGenerator + maxWidthGenerator - 1) / 8+0.7*widthSvg)
            .attr('y', generatorY - (minWidthGenerator + maxWidthGenerator - 1) / 8);
        
        //Append label to mid fill generator.
        legendGroup.append('text')
            .attr('id', 'midFillGeneratorText')
            .attr('class', 'generatorFillLegend')
            .text('50%')
            .attr('font-size', '10' )
            .attr('font-family' , 'sans-serif')
            .attr('text-anchor' , 'middle')
            .attr('y', generatorTextY)
            .attr('x', widthLegend / 2+0.7*widthSvg)
        
        //Append rectangle corresponding to the full fill generator, mid size generator is used.
        legendGroup.append('rect')   
            .attr('id' , 'fullOuterFillGenerator')
            .attr('class' , 'generatorFillLegend')
            .attr('width' , (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('height' , (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('rx' , (minWidthGenerator+maxWidthGenerator)/8)
            .attr('ry' , (minWidthGenerator+maxWidthGenerator)/8)            
            .attr('fill' , '#ffffff')
            .attr('stroke' , '#000000')
            .attr('x' , 5*widthLegend/6-(minWidthGenerator+maxWidthGenerator)/4+0.7*widthSvg)
            .attr('y' , generatorY-(minWidthGenerator+maxWidthGenerator)/4);
        
        //Append rectangle for fill of full fill generator.
        legendGroup.append('rect')   
            .attr('id', 'fullInnerFillGenerator')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator-1)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator-1)/2 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator-1)/8)
            .attr('ry',(minWidthGenerator+maxWidthGenerator-1)/8)            
            .attr('fill', '#7e7e7e')
            .attr('x', 5*widthLegend/6-(minWidthGenerator+maxWidthGenerator-1)/4+0.7*widthSvg)
            .attr('y', generatorY-(minWidthGenerator+maxWidthGenerator-1)/4);
    
        //Append label for the full fill generator. 
        legendGroup.append('text')
            .attr('id', 'fullFillGeneratorText')
            .attr('class', 'generatorFillLegend')
            .text('100%')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY)
            .attr('x',5*widthLegend/6+0.7*widthSvg)
        
    } else if (generatorFeature=='reactive power') {
        //Create legend for the reactive power of generator displayed
        
        //The y coordinate of the text labels for the QMAX generator.
        var generatorY1 = textY+45;
        
        //The y coordinate of the QMAX generator, still adjusted with generator size.
        var generatorY2 = generatorY1+(minWidthGenerator+maxWidthGenerator)/2+5;
        
        //The y coordinate of the text labels for the QMIN generator.
        var generatorTextY1 = generatorY1 -5- maxWidthGenerator/2; 
        
        //The y coordinate of the QMIN generator, still adjusted with generator size.
        var generatorTextY2 = generatorY2 +15+ maxWidthGenerator/2;
        
        //Append rectangle corresponding to no fill generator for QMAX, mid size generator used.
        legendGroup.append('rect')   
            .attr('id' , 'noFillGenerator1')
            .attr('class' , 'generatorFillLegend')
            .attr('width' , (minWidthGenerator + maxWidthGenerator) / 2)
            .attr('height' , (minWidthGenerator + maxWidthGenerator) / 2)
            .attr('rx' , (minWidthGenerator + maxWidthGenerator) / 8)
            .attr('ry' ,(minWidthGenerator + maxWidthGenerator) / 8)            
            .attr('fill' , '#ffffff')
            .attr('stroke' , '#000000')
            .attr('x' , widthLegend / 6 - (minWidthGenerator + maxWidthGenerator) / 4+0.7*widthSvg)
            .attr('y' , generatorY1 - (minWidthGenerator + maxWidthGenerator) / 4);
        
        //Append label for QMAX no fill generator.
        legendGroup.append('text')
            .attr('id', 'noFillGeneratorText1')
            .attr('class', 'generatorFillLegend')
            .text('0')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY1)
            .attr('x',widthLegend/6+0.7*widthSvg)
        
        //Append rectangle corresponding to no fill generator for QMIN, mid size generator used.
        legendGroup.append('rect')   
            .attr('id', 'noFillGenerator2')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator)/2)
            .attr('height' , (minWidthGenerator+maxWidthGenerator)/2)
            .attr('rx',(minWidthGenerator+maxWidthGenerator)/8)
            .attr('ry',(minWidthGenerator+maxWidthGenerator)/8)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/6-(minWidthGenerator+maxWidthGenerator)/4+0.7*widthSvg)
            .attr('y', generatorY2-(minWidthGenerator+maxWidthGenerator)/4);
        
        //Append label for QMIN no fill generator.
        legendGroup.append('text')
            .attr('id', 'noFillGeneratorText2')
            .attr('class', 'generatorFillLegend')
            .text('0')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY2)
            .attr('x',widthLegend/6+0.7*widthSvg)
        
        //Append rectangle corresponding to mid fill generator for QMAX, mid size generator used.
        legendGroup.append('rect')   
            .attr('id', 'midOuterFillGenerator1')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator)/8)
            .attr('ry',(minWidthGenerator+maxWidthGenerator)/8)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/2-(maxWidthGenerator+minWidthGenerator)/4+0.7*widthSvg)
            .attr('y', generatorY1 - (maxWidthGenerator+minWidthGenerator)/4);
        
        //Append inner mid fill rectangle for QMAX.
        legendGroup.append('rect')   
            .attr('id', 'midInnerFillGenerator1')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator-1)/4 )
            .attr('height', (minWidthGenerator+maxWidthGenerator-1)/4 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator-1)/16)
            .attr('ry',(minWidthGenerator+maxWidthGenerator-1)/16)            
            .attr('fill', '#6699CC')
            .attr('x', widthLegend/2-(minWidthGenerator+maxWidthGenerator-1)/8+0.7*widthSvg)
            .attr('y', generatorY1-(minWidthGenerator+maxWidthGenerator-1)/8);
        
        //Append label for QMAX mid fill generator.
        legendGroup.append('text')
            .attr('id', 'midFillGeneratorText1')
            .attr('class', 'generatorFillLegend')
            .text('0.5 \u00B7 QMIN')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY1)
            .attr('x',widthLegend/2+0.7*widthSvg)
        
        //Append rectangle corresponding to mid fill generator for QMIN, mid size generator used.
        legendGroup.append('rect')   
            .attr('id', 'midOuterFillGenerator1')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator)/8)
            .attr('ry',(minWidthGenerator+maxWidthGenerator)/8)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/2-(maxWidthGenerator+minWidthGenerator)/4+0.7*widthSvg)
            .attr('y', generatorY2 - (maxWidthGenerator+minWidthGenerator)/4);
        
        //Append inner mid fill rectangle for QMIN.
        legendGroup.append('rect')   
            .attr('id', 'midInnerFillGenerator2')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator-1)/4 )
            .attr('height', (minWidthGenerator+maxWidthGenerator-1)/4 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator-1)/16)
            .attr('ry',(minWidthGenerator+maxWidthGenerator-1)/16)            
            .attr('fill', '#C67171')
            .attr('x', widthLegend/2-(minWidthGenerator+maxWidthGenerator-1)/8+0.7*widthSvg)
            .attr('y', generatorY2-(minWidthGenerator+maxWidthGenerator-1)/8);
        
        //Append label for QMIN mid fill generator.
        legendGroup.append('text')
            .attr('id', 'midFillGeneratorText2')
            .attr('class', 'generatorFillLegend')
            .text('0.5 \u00B7 QMAX')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY2)
            .attr('x',widthLegend/2+0.7*widthSvg)
        
        //Append rectangle corresponding to full fill generator for QMAX, mid size generator used.
        legendGroup.append('rect')   
            .attr('id', 'fullOuterFillGenerator1')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator)/8)
            .attr('ry', (minWidthGenerator+maxWidthGenerator)/8)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', 5*widthLegend/6-(minWidthGenerator+maxWidthGenerator)/4+0.7*widthSvg)
            .attr('y', generatorY1-(minWidthGenerator+maxWidthGenerator)/4);
        
        //Append inner full fill rectangle for QMAX.
        legendGroup.append('rect')   
            .attr('id', 'fullInnerFillGenerator1')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator-1)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator-1)/2 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator-1)/8)
            .attr('ry',(minWidthGenerator+maxWidthGenerator-1)/8)            
            .attr('fill', '#6699CC')
            .attr('x', 5*widthLegend/6-(minWidthGenerator+maxWidthGenerator-1)/4+0.7*widthSvg)
            .attr('y', generatorY1-(minWidthGenerator+maxWidthGenerator-1)/4);
    
        //Append label for QMAX full fill generator.
        legendGroup.append('text')
            .attr('id', 'fullFillGeneratorText1')
            .attr('class', 'generatorFillLegend')
            .text('QMIN')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY1)
            .attr('x',5*widthLegend/6+0.7*widthSvg)  
        
        //Append rectangle corresponding to full fill generator for QMIN, mid size generator used.
        legendGroup.append('rect')   
            .attr('id', 'fullOuterFillGenerator2')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator)/2 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator)/8)
            .attr('ry', (minWidthGenerator+maxWidthGenerator)/8)            
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', 5*widthLegend/6-(minWidthGenerator+maxWidthGenerator)/4+0.7*widthSvg)
            .attr('y', generatorY2-(minWidthGenerator+maxWidthGenerator)/4);
        
        //Append inner full fill rectangle for QMIN.
        legendGroup.append('rect')   
            .attr('id', 'fullInnerFillGenerator2')
            .attr('class', 'generatorFillLegend')
            .attr('width', (minWidthGenerator+maxWidthGenerator-1)/2 )
            .attr('height', (minWidthGenerator+maxWidthGenerator-1)/2 )
            .attr('rx', (minWidthGenerator+maxWidthGenerator-1)/8)
            .attr('ry',(minWidthGenerator+maxWidthGenerator-1)/8)            
            .attr('fill', '#C67171')
            .attr('x', 5*widthLegend/6-(minWidthGenerator+maxWidthGenerator-1)/4+0.7*widthSvg)
            .attr('y', generatorY2-(minWidthGenerator+maxWidthGenerator-1)/4);
    
        //Append label for QMIN full fill generator.
        legendGroup.append('text')
            .attr('id', 'fullFillGeneratorText2')
            .attr('class', 'generatorFillLegend')
            .text('QMAX')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', generatorTextY2)
            .attr('x',5*widthLegend/6+0.7*widthSvg)  
        
    } 
    
}


function createInnerRectangles () {
    /*
    This function creates inner rectangles for the generator to show the level of utilization.
    */
    
    d3.selectAll('.generator').each(function (d, i) {          
                
            d3.select('#generator'+i).append('rect')
                .attr('class','innerGenerator')
                .attr('id','innerGenerator'+i);
        
    });
    
}


function innerSizeGeneratorRelativeP () {
    /*
    This function changes the color of the generators in accordance to the percentage PG is of PMAX.
    */
    
    //Array of integer containg PG for all the generator at current time. 
    var PG = new Array(numberOfGenerator);
    
    //Array of integer containing PMAX for all the generator at current time. 
    var PMAX = new Array(numberOfGenerator);
    
    /*
    Define a scale for the width of the generator. The minimum PMAX corresponds to a width of minWidthGenerator, and the maximum PMAX to a width of maxWidthGenerator.
    */
    var sizeScaleGenerator = d3.scale.linear().domain([minPMAX,maxPMAX]).range([minWidthGenerator,maxWidthGenerator]);
    
    
    d3.selectAll('.generator').each(function (d, i) {            
                
        //Integer keeping track of PG of current generator. 
        var tempPG;
                
        //Integer keeping track of PMAX of current generator. 
        var tempPMAX; 
        
        //Get width and height of generator from previously defined scale.
        var generatorWidthAndHeight = sizeScaleGenerator(interfaceData.gen[i].features[3].data[currentTime]);
        
        //Calculate current time value. 
        currentTime=Math.round(slider.value());
                
        //Find PG and PMAX of generator. 
        tempPG = interfaceData.gen[i].features[0].data[currentTime];
        tempPMAX = interfaceData.gen[i].features[3].data[currentTime];
                
                
        //Update PG array with tempPG of current generator.  
        PG[i] = tempPG;
                
        //Update PMAX with temp PMAX of current generator. 
        PMAX[i] = tempPMAX;
            
        //Check whether current PMAX is zero.
        if (PMAX[i] == 0){
            
            //Set color to lowest on scale.    
            innerGeneratorWidthAndHeight =0;
            
        } else {
                
            //Calculate the stroke width of the generator. This will be substracted from the max inner generator widht and height.
            var strokeWidth =d3.select('#generator'+i).style('stroke-width').match(/\d+/)[0];
                
            //Set color of generator using the previously defined color scale. 
            innerGeneratorWidthAndHeight = (PG[i]/PMAX[i]*(generatorWidthAndHeight-strokeWidth));
            
        }
            
        //Adjust attributes of inner generator
        d3.select('#innerGenerator'+i)                
            .attr('width', innerGeneratorWidthAndHeight)
            .attr('height',innerGeneratorWidthAndHeight)
            .attr( 'transform','translate('+  (-innerGeneratorWidthAndHeight/2 )+ ','+ (-(generatorWidthAndHeight)/2-innerGeneratorWidthAndHeight/2) +')')
            .attr('rx', innerGeneratorWidthAndHeight/4)
            .attr('ry', innerGeneratorWidthAndHeight/4)
            .style('fill', '#7e7e7e');
           
    });

}


function innerSizeGeneratorRelativeQ () {
    /*
    This function changes the color of the generators in accordance to the percentage QG is of QMIN or QMAX, depending on the sign.
    */
    
    //Array of integer containg QG for all the generator at current time. 
    var QG = new Array(numberOfGenerator);
    
    //Array of integer containing QMAX for all the generator at current time. 
    var QMAX = new Array(numberOfGenerator);
    
    //Array of integer containing QMIN for all the generator at current time. 
    var QMIN = new Array(numberOfGenerator);
    
    /*
    Define a scale for the width of the generator. The minimum QMAX corresponds to a width of minWidthGenerator, and the maximum QMAX to a width of maxWidthGenerator.
    */
    var sizeScaleGenerator = d3.scale.linear().domain([minQMAX,maxQMAX]).range([minWidthGenerator,maxWidthGenerator]);
    
    d3.selectAll('.generator').each(function (d, i) {            
                
        //Integer keeping track of QG of current generator. 
        var tempQG;
                
        //Integer keeping track of QMAX of current generator. 
        var tempQMAX; 
        
        //Integer keeping track of QMIN of current generator. 
        var tempQMIN; 
        
        //String that keeps track of the color of the inner rectangle. The color corresponds to the sign of QD. 
        var innerGeneratorColor; 
        
        //Get width and height of generator from previously defined scale.
        var generatorWidthAndHeight = sizeScaleGenerator(interfaceData.gen[i].qmax);
        
        //Calculate current time value. 
        currentTime=Math.round(slider.value());
                
        //Find QG, QMAX and QMIN of generator. 
        tempQG = interfaceData.gen[i].features[1].data[currentTime];
        tempQMAX = interfaceData.gen[i].qmax;
        tempQMIN = interfaceData.gen[i].qmin;
            
        //Update QG array with tempQG of current generator.  
        QG[i] = tempQG;
        
        //Update QMIN with tempQMIN of current generator. 
        QMIN[i] = tempQMIN;
                
        //Update QMAX with tempQMAX of current generator. 
        QMAX[i] = tempQMAX;        
        
        if (tempQG >= 0){
                
            //Set color to a blue if QD is positive
            innerGeneratorColor = '#C67171';
                
            //Check whether tempQMAX is zero.
            if (QMAX[i] == 0){
            
                //Set color to lowest on scale.    
                innerGeneratorWidthAndHeight =0;
            
            } else {
                    
                //Find stroke width of current generator
                var strokeWidth =d3.select('#generator'+i).style('stroke-width').match(/\d+/)[0];
                    
                //Define inner generator widht and height as level of utlization. 
                innerGeneratorWidthAndHeight = (QG[i]/QMAX[i]*(generatorWidthAndHeight-strokeWidth));
            
            }
                
        } else {
                
             //Set color to a red if QD is negative
            innerGeneratorColor = '#6699CC';
                
            //Check whether tempQMIN is zero.
            if (QMIN[i] == 0){
            
                //Set color to lowest on scale.    
                innerGeneratorWidthAndHeight =0;
            
            } else {
                    
                //Find stroke width of current generator
                var strokeWidth =d3.select('#generator'+i).style('stroke-width').match(/\d+/)[0];
            
                //Define inner generator widht and height as level of utlization. 
                innerGeneratorWidthAndHeight = (QG[i]/QMIN[i]*(generatorWidthAndHeight-strokeWidth));
            
            }
                
        }
            
        //Adjust attribustes of inner rectangle.
        d3.select('#innerGenerator'+i)                
            .attr('width', innerGeneratorWidthAndHeight)
            .attr('height',innerGeneratorWidthAndHeight)
            .attr( 'transform','translate('+  -innerGeneratorWidthAndHeight/2+ ','+ (-generatorWidthAndHeight/2-innerGeneratorWidthAndHeight/2) +')')
            .attr('rx', innerGeneratorWidthAndHeight/4)
            .attr('ry', innerGeneratorWidthAndHeight/4)
            .style('fill', innerGeneratorColor);
        
    });
    
}


function generatorSizeP () {
    /*
    This function changes the size of the generator in accrodance to PMAX.
    */
    
    //Only change width and height is minPMAX and maxPMAX are not identical. If they are identical, do nothing. 
    if (minPMAX != maxPMAX){
        
        /*
        Define a scale for the width of the generator. The minimum PMAX corresponds to a width of minWidthGenerator, and the maximum PMAX to a width of maxWidthGenerator.
        */
        var sizeScaleGenerator = d3.scale.linear().domain([minPMAX,maxPMAX]).range([minWidthGenerator,maxWidthGenerator]);
        
        //Calculate current time value. 
        currentTime=Math.round(slider.value());
        
        d3.selectAll('.outerGenerator').each(function (d, i) {
            
            //Get width and height of generator from previously defined scale.
            var generatorWidthAndHeight = sizeScaleGenerator(interfaceData.gen[i].features[3].data[currentTime]);
                    
            //Update height and width of generator.
            d3.select(this)
                .attr('width',generatorWidthAndHeight)
                .attr('height',generatorWidthAndHeight)
                .attr( 'transform','translate('+  (-generatorWidthAndHeight/2 ) + ','+ (-generatorWidthAndHeight) +')')
                .attr('rx', generatorWidthAndHeight/4)
                .attr('ry', generatorWidthAndHeight/4);
            
        });
        
    }
    if ($('#legendButton').text().trim() == 'Close Legend'){
        
    //Create legend for generator size
    generatorLegend();
    }
    
 }


function generatorSizeQ (){
    /*
    This function changes the size of the generator in accrodance to QMAX.
    */
     
    //This float keeps track of QMAX of the current generator.
    var tempQMAX;
     
     
    //Only change width and height is minQMAX and maxQMAX are not identical. If they are identical, do nothing. 
    if (minQMAX != maxQMAX){
        
        /*
        Define a scale for the width of the generator. The minimum QMAX corresponds to a width of minWidthGenerator, and the maximum QMAX to a width of maxWidthGenerator.
        */
        var sizeScaleGenerator = d3.scale.linear().domain([minQMAX, maxQMAX]).range([minWidthGenerator,maxWidthGenerator]);
        
        //Calculate current time value. 
        currentTime=Math.round(slider.value());
        
        d3.selectAll('.outerGenerator').each(function (d, i) {
            
            tempQMAX = interfaceData.gen[i].qmax; 
           
            //Get width and height of generator from previously defined scale.
            var generatorWidthAndHeight = sizeScaleGenerator(tempQMAX);
                    
            //Update height and width of generator.
            d3.select(this)
                .attr('width',generatorWidthAndHeight)
                .attr('height',generatorWidthAndHeight)
                .attr( 'transform','translate('+  (-generatorWidthAndHeight/2 ) + ','+ (-generatorWidthAndHeight) +')')
                .attr('rx', generatorWidthAndHeight/4)
                .attr('ry', generatorWidthAndHeight/4);
 
        });
        
    }
    
    if ($('#legendButton').text().trim() == 'Close Legend'){
        
    //Create legend for generator size
    generatorLegend();
    }
    
 }


function generatorSize () {
    /*
    This function calls on functions to change the generator size and fill generator according to the generator feature choosen.
    */
   
    if ( generatorFeature == 'real power'){
        
        //Generator size and the inner generator in correspondance to the the reactive power
        generatorSizeP();
        innerSizeGeneratorRelativeP ();
        
    } else if ( generatorFeature == 'reactive power'){
        
        //Generator size and the inner generator in correspondance to the the reactive power
        generatorSizeQ();
        innerSizeGeneratorRelativeQ ();
        
    }
   
}


function changeGeneratorFeature(genFeat){
    /*
    This function changes the generator feature displayed in the network and in the legend. 
    
    :param genFeat str: The selected generator feature. 
    */
    
    generatorFeature= genFeat;
    
    if (generatorFeature=='real power'){
        
        //Change network attributes to those corresponding to the real power
        generatorText = 'real';
        generatorSizeP();
        innerSizeGeneratorRelativeP ();
       
        //Only draw legend if legend is open
        if ($('#legendButton').text().trim() == 'Close Legend'){
            
            generatorFillLegend();
        
        }
    
    } else if (generatorFeature=='reactive power'){
        
        //Change network attributes to those corresponding to the reactive power
        generatorText = 'reactive';
        generatorSizeQ();
        innerSizeGeneratorRelativeQ ();
        
        //Only draw legend if legend is open
        if ($('#legendButton').text().trim() == 'Close Legend'){
            
            generatorFillLegend();
        
        }
        
    }
    
}


function calculateRATE_A () {
    /*
    This function calculates the lowest and highest values of RATE_A in the data set. 
    The function triggers an alert if RATE_A wasn't set.
    */
    
    //Calculate the number of branches
    numberOfBranches = interfaceData.branch.length;
    
    //This float keeps track of the RATE_A of the node that is currently viewed.
    var tempRATE_A;
    
    //This integer checks whether any RATE_A is set. If no RATE_A is set it will be false and true otherwise.
    var valueRATE_A = false;
    
    //This counter tracks how may branches have been evaluated.
    var counter = 0; 
    
    //Iterate through the branches and check whether any is not equal to zero.
    while ((valueRATE_A == false) && (counter < numberOfBranches)){
        
        tempRATE_A = interfaceData.branch[counter].rateA;
        
        if (tempRATE_A != 0){
            
            valueRATE_A = true;
            
        } else {
            
            //Remember that at least one branch has no RATE_A indicated.
            ratingUnlimited = true;
            
        }
        
        counter = counter + 1;
    }
    
    /*
    Check if all branches were evaluated, in case all branches were evalued to have no RATE_A indicated a prompt is triggered. In the other case, minRATE_A and maxRATE_A can be calculated. Zero is excluded as it indicated an unlimited rating. 
    */
    if (valueRATE_A == false) {
        
        //Define prompt which asks user to provide a lower bound for RATE_A. This will then be taken as minRATE_A and maxRATE_A.
        var lowerBoundRATE_A = parseFloat(prompt("RATE_A is not defined for any branch. Please provide a lower bound for RATE_A.","0"));
        
        if (lowerBoundRATE_A != null){
            
            //The RATE_A of all branches will be set to the value provided by the user.
            minRATE_A =lowerBoundRATE_A;
            maxRATE_A =lowerBoundRATE_A ;
            valueRATE_A =true;
            
        }
        
    } else {
        
        //Find the minimum and maximum RATE_A of the branch in te network.
        maxRATE_A = tempRATE_A;
        minRATE_A = tempRATE_A;
        
        for (i = counter+1; i < numberOfBranches; i++){
            
            tempRATE_A = interfaceData.branch[i].rateA;
            
            if (tempRATE_A != 0){
            
                maxRATE_A = Math.max(maxRATE_A,tempRATE_A);
                minRATE_A = Math.min(minRATE_A,tempRATE_A);
            
            } else {
                
                //Remember that at least one branch has no RATE_A indicated.
                ratingUnlimited = true;
                
            }
            
        }
        
    }

}


function branchLegend () {
    /*
    This function creates a legend the branch thickness.
    */
    
    //Select the legend group and white rectangle.
    var legendGroup = d3.select('#legendGroup');
    var legendRectangle = d3.select('#legendRectangle');
   
    //Remove a previously defined elements of the legend if they are still present.
    d3.selectAll('.branchLegend').remove(); 
  
    //Update width and height of the rectangle for the legend.
    widthLegend = 0.3*widthSvg;
    heightLegend = heightSvg;
    
    //The y coordinate of the title for the branch thickness legend
    var textY = 1 * (heightLegend-40) / 5 +20;
    
    //The y coordinate of the first row of branches, this is still adjusted with the actual branch thickness.
    var branch1Y = textY + 30;
    
    //The y coordinate of the text for the first row of branches.
    var branchText1Y = branch1Y + maxThicknessBranch / 2 + 15;
    
    //The y coordinate of the second row of branches, this is still adjusted with the actual branch thickness.
    var branch2Y = branch1Y + 40;
    
    //The y coordinate od the text for the second row of branches.
    var branchText2Y = branchText1Y + 40;
    
    //Create title for the branch thickness legend
    legendGroup.append('text')
        .attr('id', 'branchText')
        .attr('class', 'branchLegend')
        .text('Branch width by MVA rating A:')
        .attr('text-anchor', 'left')
        .attr("font-size", "12")
        .attr('font-family', 'sans-serif')
        .attr('y',textY)
        .attr('x',0.7*widthSvg);   
    
    //Check if branch thickness is constant
    if (minThicknessBranch == maxThicknessBranch){
        /*
        If the branch thickness is the same for all branches only one sized branch is drawn.
        */
        
        //Check whether there are no branches with an unlimited rating.
        if (ratingUnlimited == false) {
            
            //Append rectangle with thickness corresponding to the minimum thichness of the branches.
            svgLegend.append('rect')   
                .attr('id' , 'minBranch')
                .attr('class', 'branchLegend')
                .attr('width' , widthLegend / 4)
                .attr('height' ,minThicknessBranch)         
                .attr('fill' , '#ffffff')
                .attr('stroke', '#000000')
                .attr('x' , 3 * widthLegend / 8+0.7*widthSvg)
                .attr('y' , branch1Y-minThicknessBranch/2+20);
        
            //Append label for minimum branch thickness.
            svgLegend.append('text')
                .attr('id' , 'minBranchText')
                .attr('class', 'branchLegend')
                .text(round(minRATE_A,2) + ' (MVA)')
                .attr('font-size' , '10' )
                .attr('font-family' , 'sans-serif')
                .attr('text-anchor' , 'middle')
                .attr('y' , branchText1Y+20)
                .attr('x' , widthLegend / 2+0.7*widthSvg)
            
        } else {
            
            //Append rectangle with thickness corresponding to the minimum thichness of the branches.
            legendGroup.append('rect')   
                .attr('id' , 'minBranch')
                .attr('class', 'branchLegend')
                .attr('width' , widthLegend / 4)
                .attr('height' , minThicknessBranch)         
                .attr('fill' , '#ffffff')
                .attr('stroke', '#000000')
                .attr('x' , 1 * widthLegend / 8+0.7*widthSvg)
                .attr('y' , branch1Y - minThicknessBranch / 2+20);
        
            //Append label for minimum branch thickness.
            legendGroup.append('text')
                .attr('id' , 'minBranchText')
                .attr('class', 'branchLegend')
                .text(round(minRATE_A,2) + ' (MVA)')
                .attr('font-size' , '10' )
                .attr('font-family' , 'sans-serif')
                .attr('text-anchor' , 'middle')
                .attr('y' , branchText1Y +20)
                .attr('x' , widthLegend / 4+0.7*widthSvg)
            
            /*
            Append rectangle with thickness corresponding to the minimum thichness of the branches and a dashed stroke to indicate that the rating is not set.
            */
            legendGroup.append('rect')   
                .attr('id' , 'noRateBranch')
                .attr('class', 'branchLegend')
                .attr('width' , widthLegend / 4)
                .attr('height' , minThicknessBranch)         
                .attr('fill' , '#ffffff')
                .attr('stroke' , '#000000')
                .attr('x' , 5 * widthLegend / 8+0.7*widthSvg)
                .attr('y' , branch1Y - minThicknessBranch / 2+20)
                .attr('stroke-dasharray' , '8,2');
        
            //Append label for no rating branch.
            legendGroup.append('text')
                .attr('id' , 'noRateText')
                .attr('class', 'branchLegend')
                .text('no RATE_A provided')
                .attr('font-size' , '10' )
                .attr('font-family' , 'sans-serif')
                .attr('text-anchor' , 'middle')
                .attr('y' , branchText1Y+20)
                .attr('x' , 3 * widthLegend / 4+0.7*widthSvg)           
            
        }   
        
    } else {
        
        //Append rectangle with thickness corresponding to the minimum thichness of the branches.
        legendGroup.append('rect')   
            .attr('id', 'minBranch')
            .attr('class', 'branchLegend')
            .attr('width', widthLegend/4)
            .attr('height',minThicknessBranch)         
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/8+0.7*widthSvg)
            .attr('y', branch1Y-minThicknessBranch/2);
        
        //Append label for minimum thickness branch
        legendGroup.append('text')
            .attr('id', 'minBranchText')
            .attr('class', 'branchLegend')
            .text( round(minRATE_A,2)+ ' (MVA)')
            .attr("font-size", '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', branchText1Y)
            .attr('x',widthLegend/4+0.7*widthSvg)
        
        //Append rectangle with thickness corresponding to the medium thichness of the branches.
        legendGroup.append('rect')   
            .attr('id', 'midBranch')
            .attr('class', 'branchLegend')
            .attr('width', widthLegend/4)
            .attr('height',(minThicknessBranch+maxThicknessBranch)/2)         
            .attr('fill', '#ffffff')
            .attr('stroke', '#000000')
            .attr('x', widthLegend/8+0.7*widthSvg)
            .attr('y', branch2Y-(minThicknessBranch+maxThicknessBranch)/4);
        
        //Append label for medium thickness branch
        legendGroup.append('text')
            .attr('id', 'midBranchText')
            .attr('class', 'branchLegend')
            .text( round((minRATE_A+maxRATE_A)/2,2)+ ' (MVA)')
            .attr('font-size', '10' )
            .attr('font-family', 'sans-serif')
            .attr('text-anchor','middle')
            .attr('y', branchText2Y)
            .attr('x',widthLegend/4+0.7*widthSvg)
        
        //Append rectangle with thickness corresponding to the maximum thichness of the branches.
        legendGroup.append('rect')   
            .attr('id' , 'maxBranch')
            .attr('class', 'branchLegend')
            .attr('width' , widthLegend/4)
            .attr('height' , maxThicknessBranch)         
            .attr('fill' , '#ffffff')
            .attr('stroke' , '#000000')
            .attr('x' , 5*widthLegend/8+0.7*widthSvg)
            .attr('y' , branch1Y-maxThicknessBranch/2);
        
        //Append label for maximum thickness branch
        legendGroup.append('text')
            .attr('id' , 'maxBranchText')
            .attr('class', 'branchLegend')
            .text(round(maxRATE_A,2) + ' (MVA)')
            .attr('font-size' , '10' )
            .attr('font-family' , 'sans-serif')
            .attr('text-anchor' ,'middle')
            .attr('y' , branchText1Y)
            .attr('x' , 3 * widthLegend / 4+0.7*widthSvg)
        
        //Check if any rating is unlimited. If this is the case a further branch is added to the legend.
        if (ratingUnlimited == true) {  
            
            /*
            Append rectangle with thickness corresponding to the minimum thichness of the branches and a dashed stroke to indicate that the rating is not set.
            */
            legendGroup.append('rect')   
                .attr('id', 'noRateBranch')
                .attr('class', 'branchLegend')
                .attr('width', widthLegend/4)
                .attr('height',minThicknessBranch)         
                .attr('fill', '#ffffff')
                .attr('stroke', '#000000')
                .attr('x', 5*widthLegend/8+0.7*widthSvg)
                .attr('y', branch2Y-minThicknessBranch/2)
                .attr('stroke-dasharray','8,2');
        
            //Append label for no rating branch
            legendGroup.append('text')
                .attr('id', 'noRateText')
                .attr('class', 'branchLegend')
                .text('no RATE_A provided')
                .attr("font-size", '10' )
                .attr('font-family', 'sans-serif')
                .attr('text-anchor','middle')
                .attr('y', branchText2Y)
                .attr('x',3*widthLegend/4+0.7*widthSvg)
            
        }
        
    }
    
}


function colorBranchRelative () {
    /*
    This function colors in the branches relative to the RATE_A. Furthermore it is made apparent when RATE_A isn't met. 
    */

    //Set scale for color of branches, scale goes from zero to 1. 
    var colorScaleBranch = d3.scale.linear().domain([0, 0.25, 0.5, 0.75, 1]).range(['#3f5ea9','#a3d1e5','#f9f8c2','#fa9d5b','#c31e28']);

    //This array of floats keeps track of the current apparent power calculated.
    var S = new Array(numberOfBranches);
    
    /*
    This array of floats keeps track of the RATE_A of the branches. If the rating is set to zero, then the corresponding element in the array will be set to the minimum RATE_A excluding zero.
    */
    var RATE_A = new Array(numberOfBranches);
    
    //Array of boolean. If the rating is exceeded for branch i, element i-1 is marked as true. 
    var criticallyHighBranch = new Array(numberOfBranches); 
    
    //Define diagonal hash pattern for when RATE_A is not met
    var patternHigh = svg.append('defs')
                            .append('pattern')
                                .attr('id' , 'hashHigh4_4')
                                .attr('width', 8 )
                                .attr('height' , 8)
                                .attr('patternUnits' , 'userSpaceOnUse')
                                .attr('patternTransform','rotate(60)')
                            .append("rect")
                                .attr('width',4)
                                .attr('height',8)
                                .attr('transform','translate(0,0)')
                                .attr('fill','#c31e28' );
    
    //The current real power injected at from bus end
    var tempPF; 
        
    //The current reactive power injected at from bus end
    var tempQF; 
        
    //The current real power injected at from bus end
    var tempPT; 
        
    //The current reactive power injected at from bus end
    var tempQT;
        
    //This float keeps track of the current apparent power of the evaluated branch.
    var tempS;
                
    //This float keeps track of the current RATE_A of the evaluated branch.
    var tempRATE_A;
    
    //Change color of branches.
    d3.selectAll('.topLinkRectangle').style('fill', function(d,i) {

         
        
        //Calculate current time value. 
        currentTime=Math.round(slider.value());
                
        //Extract values required to calculate tempRate
        tempPF = interfaceData.branch[i].features[2].data[currentTime];
        tempQF = interfaceData.branch[i].features[3].data[currentTime];
        tempPT = interfaceData.branch[i].features[4].data[currentTime];
        tempQT = interfaceData.branch[i].features[5].data[currentTime];
        
        //Calculate current apparent power
        tempS = Math.sqrt(Math.pow((tempPF-tempPT), 2)+Math.pow((tempQF-tempQT), 2));
         
        
                                
        //Set tempRATE_A to the minRATE_A if it is zero. Otherwise take value indicated by the data.
        if (interfaceData.branch[i].rateA == 0){
            
            tempRATE_A = minRATE_A;        
            

        } else {
            
            tempRATE_A = interfaceData.branch[i].rateA;
            
        }
        
                
        //Check whether tempS exceeds tempRATE_A
        if (tempS > tempRATE_A) {
            
            //Set tempS to tempRATE_A in order to not exceed RATE_A on color scale.
            tempS = tempRATE_A;
            
            //Set critically high indicator of this branch to true. 
            criticallyHighBranch[i] = true;
            
        } else {
                    
            //Set critically high indicator of this branch to false. 
            criticallyHighBranch[i] = false;
                    
        }
                
        //Update S array with tempS of current branch.  
        S[i] = tempS;
                
        //Update RATE_A with tempRATE_A of current branch. 
        RATE_A[i] = tempRATE_A;
        
        //Check whether tempRate is zero.
        if (S[i] == 0) {
            
            //Set color to lowest on scale.
            return colorScaleBranch(0);
            
        } else if  (criticallyHighBranch[i] == false){
            
            //Set color of branch using the previously defined color scale. 
            return colorScaleBranch(S[i]/RATE_A[i]);
            
        } else {
            
           return 'url(#hashHigh4_4)';
            
        }
 
    });
 
}


function branchThickness () {
    /*
    This function changes the thickness of the branch in correspondance to the RATE_A. Futhermore, this function marks the branches where no rating is set. The rectangle are also transformed in this function.
    */
        
    /*
    Define a scale for the thickness of the branches. The minimum RATE_A corresponds to a thickness of minThicknessBranch, and the max to a thickness of maxThicknessBranch. 
    */
    var thicknessScaleBranch = d3.scale.linear().domain([minRATE_A,maxRATE_A]).range([minThicknessBranch,maxThicknessBranch]);
        
    //The id of the top rectangle of the branch that is currently evaluated
    var topIDText;
        
    //The top rectangle of the branch that is currently evaluated
    var topRect;
       
    //The id of the bottom rectangle of the branch that is currently evaluated
    var bottomIDText;
    
    //The bottom rectangle of the branch that is currently evaluated
    var bottomRect;

    //This float keeps track of the RATE_A of the current branch.
    var tempRATE_A;
    
    //This float keeps track of the current calculated thickness. 
    var tempThickness;          
        
    //This float keeps track of the current calculated length of the rectangle. 
    var tempRectangleLength;
        
    //This float keeps track of the x value of the starting poingof the current line.
    var tempX1;
        
    //This float keeps track of the x value of the ending poing value of the current line.
    var tempX2;
        
    //This float keeps track of the y value of the starting poing of the current line.
    var tempY1;
        
    //This float keeps track of the y value of the ending poing of the current line.
    var tempY2;
        
    //This float keeps track difference between the x values of the end and staring point of the line.
    var tempDeltaX;
        
    //This float keeps track difference between the y values of the end and staring point of the line.
    var tempDeltaY;
        
    //This float keeps track of the angle by which the rectangle has to be transformed.
    var tempRotationAngle;
    
    //The rotation text for the current rectangle
    var tempRotationText;
        
    //The transformation text for the current rectangle
    var tempTransformationText;
    
    d3.selectAll('.link').each(function (d) {
            
        //Select upper branch rectangle
        topIDText= '#r' + parseInt(d.id);           
        topRect = d3.select(topIDText);
            
        //Select bottom branch rectangle
        bottomIDText= '#bottomLinkRectangle' + parseInt(d.id);
        bottomRect = d3.select(bottomIDText);
        
        tempRATE_A = interfaceData.branch[d.id].rateA;
            
        //Define thickness of branch
        if (minRATE_A != maxRATE_A){
            
            if (tempRATE_A == 0){
                
                tempThickness = thicknessScaleBranch(minRATE_A);
                
                //Make the outline of the rectangle dashed when no RATE_A is provided for the branch
                topRect.attr('stroke-dasharray','8,2');           
            
            } else {
            
                tempThickness = thicknessScaleBranch(tempRATE_A);
            
            }
            
        } else {
            
            
            if ((minThicknessBranch != 10) || (maxThicknessBranch != 10)){
                
                //Set min and max thickness to same value if RATE_A constant
                minThicknessBranch =10; 
                maxThicknessBranch =10;
                
                //Draw legend
                branchLegend ();
                
            }
            
            
            if (tempRATE_A == 0){
                
                tempThickness = 10;
                
                //Make the outline of the rectangle dashed when no RATE_A is provided for the branch
                topRect.attr('stroke-dasharray','8,2');           
            
            } else {
            
                tempThickness = 10;
            
            }
            
        }
            
        //Set values for starting and end point of the current branch
        tempX1 = parseFloat(d3.select(this).attr('x1'));
        tempY1 = parseFloat(d3.select(this).attr('y1'));
        tempX2 = parseFloat(d3.select(this).attr('x2'));
        tempY2 = parseFloat(d3.select(this).attr('y2'));
            
        //Calculate delta values of the lines
        tempDeltaX = (tempX2-tempX1);
        tempDeltaY = (tempY2-tempY1);
        
        //Calcualte length of line
        tempRectangleLength = Math.sqrt(Math.pow((tempX2- tempX1), 2) + Math.pow((tempY2-tempY1), 2));
        
        //Calcualte the angle by which the rectangle has to be rotated
        if(tempDeltaY< 0 && tempDeltaX >= 0){
                
        tempRotationAngle  = Math.acos(-tempDeltaY/tempRectangleLength);
        tempRotationAngle  = tempRotationAngle*(180/Math.PI)-90;
                
        } else if (tempDeltaY < 0 && tempDeltaX < 0){
                
            tempRotationAngle  = Math.acos(-tempDeltaY/tempRectangleLength);
            tempRotationAngle = -tempRotationAngle*(180/Math.PI)-90;
                
        } else if (tempDeltaY >= 0 && tempDeltaX < 0){
                
            tempRotationAngle = Math.acos(-tempDeltaY/tempRectangleLength);
            tempRotationAngle = -tempRotationAngle*(180/Math.PI)+180+90;
                
        } else {
            
            tempRotationAngle = Math.acos(-tempDeltaY/tempRectangleLength);
            tempRotationAngle = tempRotationAngle*(180/Math.PI)-90;
            
        }
            
        //Set the text for the transformation of the rectangles
        tempRotationText = 'rotate(' + parseFloat(tempRotationAngle) + ' ' + parseFloat(tempX1) + ' ' + parseFloat(tempY1) + ')';
        tempTransformationText = tempRotationText + ' translate(0,'+(-tempThickness/2)+')';
            
        //Change attributes of top rectangle
        topRect
            .attr('x',tempX1)
            .attr('y',tempY1)
            .attr('width' ,tempRectangleLength)
            .attr('height',tempThickness)
            .attr('stroke','black')
            .attr('transform', tempTransformationText);
            
        //Change attributes of bottom rectangle, height is adjusted for stroke width.
        bottomRect
            .attr('x',tempX1)
            .attr('y',tempY1)
            .attr('width' ,tempRectangleLength)
            .attr('height',tempThickness-0.5)
            .attr('stroke','black')
            .attr('transform', tempTransformationText);
     
    });
   
}


function drawArrow () {
    /*
    This function draws two arrows in the branches to show the real power flow direction.
    */
    
    //The current real power injected at from bus end
    var tempPF; 
        
    //The current real power injected at from bus end
    var tempPT; 
    
    //The difference between the real power injected at the from and at the two bus
    var tempDeltaP;
    
    //The id of the evaluted branch
    var branchID;
    
    //The id of the evaluated branch rectangle
    var topRectIDText;
    
    //The evaluated branch rectangle
    var topRect;
    
    //The id of the first arrow of the evaluated branch
    var arrow1IDText;
    
    //The first arrow of the evaluated
    var arrow1 ;
    
    //The id of the second arrow of the evaluated branch
    var arrow2IDText;
    
    //The second arrow of the evaluated branch
    var arrow2 ;
    
    //This float keeps track of the x value of the starting poingof the current line.
    var tempX1;
        
    //This float keeps track of the x value of the ending poing value of the current line.
    var tempX2;
        
    //This float keeps track of the y value of the starting poing of the current line.
    var tempY1;
        
    //This float keeps track of the y value of the ending poing of the current line.
    var tempY2;
    
    //This float keeps track difference between the x values of the end and staring point of the line.
    var tempDeltaX;
        
    //This float keeps track difference between the y values of the end and staring point of the line.
    var tempDeltaY;
    
    //The x coordinate of the first arrow along the branch line
    var xArrow1;
    
    //The y coordinate of the first arrow along the branch line
    var yArrow1;
    
    //The x coordinate of the second arrow along the branch line
    var xArrow2;
    
    //The y coordinate of the second arrow along the branch line
    var yArrow2;
    
    //The slope of the evaluated branch line
    var slope; 
    
    //The slope perpendicular to the one of the branch
    var slopePrependicular;
    
    //The direction vector of the line of the branch
    var u1; 
    
    //The direction vector of the perpendicular to the line of the branch
    var u2; 
    
    //The reference point for the first arrow
    var pArrow1;
    
    //The reference point of the second arrow
    var pArrow2;
    
    //The thickness of the rectangle of the evaluated branch
    var tempThickness ;
    
    //The length of the rectangle of the evaluated branch
    var tempLength;
    
    //The parameters for the prependicular
    var lamda1Positive ;        
    var lamda1Negative ;
    
    //The parameters for the parallel
    var lamda1aPositive;
    var lamda2Positive;
    
    //The coordinates of the first arrow
    var x1A;
    var y1A;
    var x1B;
    var y1B;
    var x1C;
    var y1C; 
    
    //The coordinates of the second arrow
    var x2A;
    var y2A;
    var x2B;
    var y2B;
    var x2C;
    var y2C; 
    
    //Integer that is either 1 or -1, to determine the arrow direction depending on the flow direction
    var sign;

        
    d3.selectAll('.link').each(function(d,i){
        
        //Calculate current time value. 
        currentTime=Math.round(slider.value());
                
        //Extract values for the real power 
        tempPF = interfaceData.branch[i].features[2].data[currentTime];       
        tempPT = interfaceData.branch[i].features[4].data[currentTime];
        tempDeltaP = (tempPF-tempPT);
        
        //Select all relevant elements
        branchID = d.id;
        topRectIDText = '#r' + parseInt(branchID);
        topRect = d3.select(topRectIDText);
        arrow1IDText = '#arrow1' + parseInt(branchID);
        arrow1 = d3.select(arrow1IDText);
        arrow2IDText = '#arrow2' + parseInt(branchID);
        arrow2 = d3.select(arrow2IDText);
        
        
        //Get starting and end coordinates of the line as well as the difference between the coordinates
        tempX1 = parseFloat(d3.select(this).attr('x1'));
        tempY1 = parseFloat(d3.select(this).attr('y1'));        
        tempX2 = parseFloat(d3.select(this).attr('x2'));
        tempY2 = parseFloat(d3.select(this).attr('y2'));
        tempDeltaX =  parseFloat(tempX2-tempX1);
        tempDeltaY =  parseFloat(tempY2-tempY1);
        
        //Get x and y corrdinates of the arrows on the line
        yArrow1 = tempY1 + (tempY2-tempY1)/3;
        xArrow1 = tempX1 + (tempX2-tempX1)/3;
        yArrow2 = tempY1+2*(tempY2-tempY1)/3;
        xArrow2 = tempX1+2*(tempX2-tempX1)/3;

        //Calculate slope of line
        slope =(tempY2-tempY1)/(tempX2-tempX1);
        
        //Calculate slope perpendicular to lien
        slopePrependicular =-1/(slope);
     
        //Define direction vectors
        u1 = [1, slopePrependicular];
        u2 = [1, slope];
        
        //Define the reference points for the arrows
        pArrow1 = [xArrow1, yArrow1];
        pArrow2 = [xArrow2, yArrow2];
        
        //Get thickness and length of rectangle
        tempThickness = topRect.attr('height');
        tempLength= topRect.attr('width');
        
        //Define parameters for arrows
        lamda1Positive = Math.sqrt(Math.pow(((tempThickness-1)/2),2)/(1+Math.pow(slopePrependicular,2)));        
        lamda1Negative =- lamda1Positive ;        
        lamda1aPositive = Math.sqrt(Math.pow(3.5,2)/(1+Math.pow(slope,2)));
        lamda2Positive = Math.sqrt(Math.pow(3.5,2)/(1+Math.pow(slope,2)));
        
        //Find flow direction
        if (tempDeltaP > 0){
            
            sign = 1;
            
        } else {
            
            sign =-1;
            
        }
        
        //Define start, middle and ending point of the two arrows.
        if ((tempDeltaY >= 0) && (tempDeltaX >= 0)){
            
            x1A = pArrow1[0] + lamda1Positive * u1[0] - sign * lamda1aPositive * u2[0];
            y1A = pArrow1[1] + lamda1Positive * u1[1] - sign * lamda1aPositive * u2[1];
            x1B = pArrow1[0] + sign * lamda2Positive*u2[0];
            y1B = pArrow1[1] + sign * lamda2Positive*u2[1];
            x1C = pArrow1[0] + lamda1Negative * u1[0] - sign * lamda1aPositive * u2[0];       
            y1C = pArrow1[1] + lamda1Negative * u1[1] - sign * lamda1aPositive * u2[1];
            x2A = pArrow2[0] + lamda1Positive * u1[0] - sign * lamda1aPositive * u2[0];
            y2A = pArrow2[1] + lamda1Positive * u1[1] - sign * lamda1aPositive * u2[1];
            x2B = pArrow2[0] + sign * lamda2Positive*u2[0];
            y2B = pArrow2[1] + sign * lamda2Positive*u2[1];
            x2C = pArrow2[0] + lamda1Negative * u1[0] - sign * lamda1aPositive * u2[0];
            y2C = pArrow2[1] + lamda1Negative * u1[1] - sign * lamda1aPositive * u2[1];  
            
        } else if (((tempDeltaY >= 0) && (tempDeltaX < 0))) {
            x1A = pArrow1[0] + lamda1Positive * u1[0] + sign * lamda1aPositive * u2[0];
            y1A = pArrow1[1] + lamda1Positive * u1[1] + sign * lamda1aPositive * u2[1];
            x1B = pArrow1[0] - sign * lamda2Positive * u2[0];
            y1B = pArrow1[1] - sign * lamda2Positive * u2[1];
            x1C = pArrow1[0] + lamda1Negative * u1[0] + sign * lamda1aPositive * u2[0];       
            y1C = pArrow1[1] + lamda1Negative * u1[1] + sign * lamda1aPositive * u2[1];
            x2A = pArrow2[0] + lamda1Positive * u1[0] + sign * lamda1aPositive * u2[0];
            y2A = pArrow2[1] + lamda1Positive * u1[1] + sign * lamda1aPositive * u2[1];
            x2B = pArrow2[0] - sign * lamda2Positive * u2[0];
            y2B = pArrow2[1] - sign * lamda2Positive * u2[1];
            x2C = pArrow2[0] + lamda1Negative * u1[0] + sign * lamda1aPositive * u2[0];
            y2C = pArrow2[1] + lamda1Negative * u1[1] + sign * lamda1aPositive * u2[1];   
            
        } else if (((tempDeltaY < 0) && (tempDeltaX >= 0))){
            
            x1A = pArrow1[0] + lamda1Positive * u1[0] - sign * lamda1aPositive * u2[0];
            y1A = pArrow1[1] + lamda1Positive * u1[1] - sign * lamda1aPositive * u2[1];
            x1B = pArrow1[0] + sign * lamda2Positive * u2[0];
            y1B = pArrow1[1] + sign * lamda2Positive * u2[1];
            x1C = pArrow1[0] + lamda1Negative * u1[0] - sign * lamda1aPositive * u2[0];       
            y1C = pArrow1[1] + lamda1Negative * u1[1] - sign * lamda1aPositive * u2[1];
            x2A = pArrow2[0] + lamda1Positive * u1[0] - sign * lamda1aPositive * u2[0];
            y2A = pArrow2[1] + lamda1Positive * u1[1] - sign * lamda1aPositive * u2[1];
            x2B = pArrow2[0] + sign * lamda2Positive * u2[0];
            y2B = pArrow2[1] + sign * lamda2Positive * u2[1];
            x2C = pArrow2[0] + lamda1Negative * u1[0] - sign * lamda1aPositive * u2[0];
            y2C = pArrow2[1] + lamda1Negative * u1[1] - sign * lamda1aPositive * u2[1];
            
        } else {
            
            x1A = pArrow1[0] + lamda1Positive * u1[0] + sign * lamda1aPositive * u2[0];
            y1A = pArrow1[1] + lamda1Positive * u1[1] + sign * lamda1aPositive * u2[1];
            x1B = pArrow1[0] - sign * lamda2Positive * u2[0];
            y1B = pArrow1[1] - sign * lamda2Positive * u2[1];
            x1C = pArrow1[0] + lamda1Negative * u1[0] + sign * lamda1aPositive * u2[0];       
            y1C = pArrow1[1] + lamda1Negative * u1[1] + sign * lamda1aPositive * u2[1];
            x2A = pArrow2[0] + lamda1Positive * u1[0] + sign * lamda1aPositive * u2[0];
            y2A = pArrow2[1] + lamda1Positive * u1[1] + sign * lamda1aPositive * u2[1];
            x2B = pArrow2[0] - sign * lamda2Positive * u2[0];
            y2B = pArrow2[1] - sign * lamda2Positive * u2[1];
            x2C = pArrow2[0] + lamda1Negative * u1[0] + sign * lamda1aPositive * u2[0];
            y2C = pArrow2[1] + lamda1Negative * u1[1] + sign * lamda1aPositive * u2[1];            
            
            
        }
        
        arrow1.attr("points", ''+x1A+','+y1A+','+ x1B+','+y1B+','+ x1C+','+y1C+'');    
        arrow2.attr("points", ''+x2A+','+y2A+','+ x2B+','+y2B+','+ x2C+','+y2C+'');    
        
    });
    
}


function calculateMaxAndMinValues () {
    /*
    This function calls on other functions to calculate the min and max values needed for the data display.
    */
    
    //Calculate bounds for VMIN and VMAX for the bus
    calculateVMINAndVMAX ();
    
    //Calculate bounds for PD and QD for the bus
    calculatePDAndQD ();
    
    //Calculate bounds for PMAX of generators
    calculatePMAX ();
    
    //Calculate bounds for QMIN and QMAX of generators
    calculateQMINAndQMAX ();
    
    //Calculate bounds for RATE_A of branches
    calculateRATE_A ();
    
}


function adjustOnWindowChange () {
    /*
    This function calls upon other functions adjusting the elements when the window size is changed.
    */
    
   
    if ($('#legendButton').text().trim() == 'Close Legend'){
        
        //Redraw legend 
        drawLegend();
    }
    
    //Center the network 
    centerGroup();
   
}


function adjustOnTimeChange () {
    /*
    This function adjust the the data display in the network to suit the current time. 
    */
    
    //Adjust color of bus
    colorBusRelative();
    
    //Adjust bus radius
    busRadius ();
    
    //Adjust generator size
    generatorSize();
    
    //Adjust the starting poing of the group of the generator
    adjustStartingPointOfGeneratorLine ();
    
    //Adjust color of branches
    colorBranchRelative();
    
}


function drawLegend () {
    /*
    This function draws the legend for  the data display in the network graph.
    */
    
    //Draw rectangle to place color legend on
    drawLegendRect ();
    
    //Draw legend for color of bus and branches
    colorLegend();
    
    //Draw legend for bus radius
    busLegend ();
    
    //Draw legend for generator size
    generatorLegend ();
    
    //Draw legend for generator fill
    generatorFillLegend ();
    
    //Draw legend for branches
    branchLegend () ;
    
}


function startUpDataDisplay () {
    /*
    This function creates the data display when the tool is first launched.
    */
    
    //Draw legend for data display in network 
    drawLegend ();
    
    //Adjust color of bus
    colorBusRelative();
    
    //Adjust bus radius
    busRadius ();
    
    //Create inner rectangles for generator
    createInnerRectangles();
    
    //Adjust generator size
    generatorSize();
    
    //Adjust the starting poing of the group of the generator
    adjustStartingPointOfGeneratorLine ();   
    
    
    //Adjust color of branches
    colorBranchRelative();
    
}


$('#legendButton').click(function(){
    
    if($(this).text().trim() == 'Close Legend'){
        
        $(this).text('Open Legend');
        
        //Remove legend elements
        $('#legendGroup').remove();
        
        //Center network
        centerGroup();
       
    }else{
        
        $(this).text('Close Legend');
        
        //Draw legend again
        drawLegend ();
        
        //Center network
        centerGroup ();
        
    }
    
});



window.onresize = function (event) {
    /*
    This function makes sure that everything is resized when the window size is changed.
    */
    
    adjustOnWindowChange ();    
    
};
