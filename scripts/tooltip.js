function round(value, exp) {
    /*
    This function rounds value to exp decimal points. It was taken from stackexchange
    (https://stackoverflow.com/questions/1726630/formatting-a-number-with-exactly-two-decimals-in-javascript/34819033).
    */

    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
    
}


function tooltipBus(){
    /*
    This function creates a tooltip for the bus. The tooltip appears on mouseover and disappears on mouseout.
    */
    
    //Define tooltip
    var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function(d) {
                    
                    //Define tooltip for bus. 
                    return '<b>Bus ' +d.id +'</b>  <br />'+ ' <br />'+'<table> <tr> <th>Property</th> <th>Value</th> <th>Units</th> </tr>  <tr> <td>VMAX</td> <td>'+interfaceData.bus[d.id-1].vmax+' </td> <td>p.u.</td> </tr> <tr> <td>VMIN</td> <td>'+interfaceData.bus[d.id-1].vmin+' </td> <td> p.u. </td> </tr> <tr> <td>BASE_KV</td> <td>'+interfaceData.bus[d.id-1].baseKV+' </td> <td> kV </td> </tr> <tr> <td>VM</td> <td>'+round(interfaceData.bus[d.id-1].features[2].data[currenttime],3)+' </td> <td>p.u.</td> </tr> <tr> <td>VA</td> <td>'+round(interfaceData.bus[d.id-1].features[3].data[currenttime],3)+' </td> <td>degrees</td> </tr> <tr> <td>PD</td> <td>'+round(interfaceData.bus[d.id-1].features[0].data[currenttime],3)+' </td> <td>MW</td> </tr> <tr> <td>QD</td> <td>'+round(interfaceData.bus[d.id-1].features[1].data[currenttime],3)+' </td> <td>MWAr</td> </tr> </table>'})
   
    svg.call(tip);  

    //Let tooltip show on mouseover and disappear on mouseout. 
    d3.selectAll('.topBus')
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);

}


function tooltipGenerator(){
    /*
    This function creates a tooltip for the generator. The tooltip appears on mouseover and disappears on mouseout. 
    */
    //Define tooltip
    var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function(d,i) {
                    
                    //Set generator ID to its place in the data plus one.
                    var genID = i+1;
                    
                    //Define values for tooltip of generator. 
                    return '<b>Generator ' +genID +'</b>  <br />'+ ' <br />'+'<table> <tr> <th>Property</th> <th>Value</th> <th>Units</th> </tr>  <tr> <td>PMAX</td> <td>'+round(interfaceData.gen[genID-1].features[3].data[currenttime], 3)+' </td> <td>MW</td> </tr> <tr> <td>PMIN</td> <td>'+round(interfaceData.gen[genID-1].features[4].data[currenttime], 3)+' </td> <td>MW</td> </tr> <tr> <tr> <td>QMAX</td> <td>'+round(interfaceData.gen[genID-1].qmax, 3)+' </td> <td>MWAr</td> </tr> <tr> <td>QMIN</td> <td>'+round(interfaceData.gen[genID-1].qmin, 3)+' </td> <td>MWAr</td> </tr> <tr> <td>PG</td> <td>'+round(interfaceData.gen[genID-1].features[0].data[currenttime], 3)+' </td> <td>MW</td> </tr> <tr> <td>QG</td> <td>'+round(interfaceData.gen[genID-1].features[1].data[currenttime], 3)+' </td> <td>MWAr</td> </tr> <tr> <td>VG</td> <td>'+round(interfaceData.gen[genID-1].features[2].data[currenttime], 3)+' </td> <td>MWAr</td> </tr> </table>'})
   
    svg.call(tip);  

    //Let tooltip show on mouseover and disappear on mouseout. 
    d3.selectAll('.generator')
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);

}


function tooltipBranch(){
    /*
    This function creates a tooltip for the branch. The tooltip appears on mouseover and disappears on mouseout. 
    */
    
   
    
    
    //Define tooltip
    var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function(d,i) {
                    
                    //Set ID of branch to its place in the data plus 1.
                    var branchID = i+1; 
                    
                    //Set values for tooltip of branch.
                    return '<b>Branch ' +branchID +'</b>  <br />'+ ' <br />'+'<table> <tr> <th>Property</th> <th>Value</th> <th>Units</th> </tr> <tr> <td>F_BUS</td> <td>'+round(interfaceData.branch[branchID-1].from,3)+' </td> <td> </td> </tr> <tr> <td>T_BUS</td> <td>'+round(interfaceData.branch[branchID-1].to,3)+' </td> <td> </td> </tr> <tr> <td>BR_R</td> <td>'+round(interfaceData.branch[branchID-1].resistance,3)+' </td> <td>p.u.</td> </tr> <tr> <td>BR_X</td> <td>'+round(interfaceData.branch[branchID-1].reactance,3)+' </td> <td>p.u.</td> </tr> <tr> <tr> <td>RATE_A</td> <td>'+round(interfaceData.branch[branchID-1].rateA,3)+' </td> <td>MVA</td> </tr> <tr> <td>RATE_B</td> <td>'+round(interfaceData.branch[branchID-1].rateB,3)+' </td> <td>MVA</td> </tr> <tr> <td>RATE_C</td> <td>'+round(interfaceData.branch[branchID-1].rateC,3)+' </td> <td>MVA</td> </tr> <tr> <td>PF</td> <td>'+round(interfaceData.branch[branchID-1].features[2].data[currenttime],3)+' </td> <td>MW</td> </tr> <tr> <td>QF</td> <td>'+round(interfaceData.branch[branchID-1].features[3].data[currenttime],3)+' </td> <td>MWAr</td> </tr> <tr> <td>PT</td> <td>'+round(interfaceData.branch[branchID-1].features[4].data[currenttime],3)+' </td> <td>MW</td> </tr> <tr> <td>QT</td> <td>'+round(interfaceData.branch[branchID-1].features[5].data[currenttime],3)+' </td> <td>MWAr</td> </tr> </table>'})
   
    svg.call(tip);  

    //Let tooltip show on mouseover and disappear on mouseout. 
    d3.selectAll('.topLinkRectangle')
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);

}
