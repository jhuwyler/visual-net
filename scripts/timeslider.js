// JavaScript Document for the time slider
"use strict";

// Function to create the timeslider. Used in the startup.
function createSlider(){
    var stepValues = new Array(numberOfTimesteps);
    for(var i=0; i<numberOfTimesteps; i++){
        stepValues[i]=i;
    }
    var timeIncrement= 1440/numberOfTimesteps;
    var zero = d3.format("02d")
    var tickFormatter = function(d){
        var rawTime = 0+timeIncrement*d;
        return zero(Math.floor(rawTime/60))+":"+zero(d3.round(rawTime%60));
    }
    //build the slider
    slider=d3.slider().min(0).max(numberOfTimesteps-1).ticks(13).showRange(true).stepValues(stepValues).tickFormat(tickFormatter);
    d3.select("#timeslider").call(slider);
    
    
    //added a callbackfunction for the slider, that if the slider moves, the currenttime gets updated and the timeindicators in the graphs are moved  
    slider.callback(function (){
        currenttime=slider.value();
        //Adjust the graph
        adjustOnTimeChange ();
        // move the red indicator lines in the graphs if there are any
        if($('#sidebar-right').is(':visible') && $('.nv-interactive').length ){
            d3.selectAll('.nv-interactive').selectAll('line').attr('transform', 'translate('+timeGraphScale(currenttime)+',0)');}
    });
}
//window resize function for the timeslider, such that it gets replotted everytime the window resizes.
nv.utils.windowResize(function() {
                    slider.destroy();
                    d3.select("#timeslider").call(slider);
                      });
//Function to play the timeslider. It also takes care of the state of the playbutton. state is a global variable
function animateSlider(){
    changePlayButton();
    var countdown = numberOfTimesteps-currenttime;
    //this private recurcive function makes sure, that each timestep is executed after the preceding one.
    function timeout() {
        if(state=='play' && countdown){
            setTimeout(function () {
                // move the slider and check if we are at the end.
                // Then recall the parent function to
                // create a recursive loop.
                countdown--;
                currenttime=currenttime+1;
                if(!(countdown)){
                    state='stop';
                    changePlayButton();  
                }
                slider.setValue(currenttime);
                timeout();
            }, timeToComplete/numberOfTimesteps);
        }
    }
    
    if(state=='stop'){
      state='play';
      timeout();
    }
    else if(state=='play'){
      state = 'stop';
    }

}


//change play button to pause button and vice versa
function changePlayButton(){
    var playButton = document.getElementById('playButton');
    if(playButton.innerHTML.includes('play')){
        playButton.innerHTML = '<i class="fas fa-pause"></i>';
    } else{
        playButton.innerHTML = '<i class="fas fa-play"></i>';
    }
}