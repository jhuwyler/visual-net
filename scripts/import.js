// Import of Data into the interface constructor

//create new dataInterface to fill in the imported data
var interfaceData = new dataInterface();

// Names and description of the features of the bus, branch and generator. The index is used to extract the right data from the imported data
var featureNamesBus = [{
    name: 'PD',
    index: 2,
    description: 'real power demand (MW)'
}, {
    name: 'QD',
    index: 3,
    description: 'reactive power demand (MVAr)'
}, {
    name: 'VM',
    index: 7,
    description: 'voltage magnitude (p.u.)'
}, {
    name: 'VA',
    index: 8,
    description: 'voltage angle (degrees)'
}, {
    name: 'LAM_P',
    index: 13,
    description: 'Lagrange multiplier on real power mismatch (u/MW)'
}, {
    name: 'LAM_Q',
    index: 14,
    description: 'Lagrange multiplier on reactive power mismatch (u/MVAr)'
}, {
    name: 'MU_VMAX',
    index: 15,
    description: 'Kuhn-Tucker multiplier on upper voltage limit (u/p.u.)'
}, {
    name: 'MU_VMIN',
    index: 15,
    description: 'Kuhn-Tucker multiplier on lower voltage limit (u/p.u.)'
}];
var featureNamesBranch = [{
    name: "TAP",
    index: 8,
    description: "transformer off nominal turns ratio"
}, {
    name: "SHIFT",
    index: 9,
    description: "transformer phase shift angle (degrees)"
}, {
    name: "PF",
    index: 13,
    description: "real power injected at from bus end (MW)"
}, {
    name: "QF",
    index: 14,
    description: "reactive power injected at from bus end (MWAr)"
}, {
    name: "PT",
    index: 15,
    description: "real power injected at to bus end (MW)"
}, {
    name: "QT",
    index: 16,
    description: "reactive power injected at to bus end (MWAr)"
}, {
    name: "MU_SF",
    index: 17,
    description: "Kuhn-Tucker multiplier on MVA limit at from bus (u/MVA)"
}, {
    name: "MU_ST",
    index: 18,
    description: "Kuhn-Tucker multiplier on MVA limit at to bus (u/MVA)"
}, {
    name: "MU_ANGMIN",
    index: 19,
    description: "Kuhn-Tucker multiplier lower angle differnce limit (u/degree)"
}, {
    name: "MU_ANGMAX",
    index: 20,
    description: "Kuhn-Tucker multiplier upper angle differnce limit (u/degree) (u/MVA)"
}];
var featureNamesGen = [{
    name: "PG",
    index: 1,
    description: "real power output (MW)"
}, {
    name: "QG",
    index: 2,
    description: "reactive power output (MWAr)"
}, {
    name: "VG",
    index: 5,
    description: "voltage magnitude setpoint (p.u.)"
}, {
    name: "PMAX",
    index: 8,
    description: "maximum real power output (MW)"
}, {
    name: "PMIN",
    index: 9,
    description: "minimum real power output (MW)"
}, {
    name: "MU_PMAX",
    index: 21,
    description: "Kuhn-Tucker multiplier on upper P_g limit (u/MW)"
}, {
    name: "MU_PMIN",
    index: 22,
    description: "Kuhn-Tucker multiplier on lower P_g limit (u/MW)"
}, {
    name: "MU_QMAX",
    index: 23,
    description: "Kuhn-Tucker multiplier on upper Q_g limit (u/MVAr)"
}, {
    name: "MU_QMIN",
    index: 24,
    description: "Kuhn-Tucker multiplier on lower Q_g limit (u/MVAr)"
}];

//array for the bustypes
var bustype = ["PQ", "PV", "ref", "isolated"];

//function to import the data in a specific format into the interface. 
function importData(json, callback) {

    // fill in the bus data
    for (var i = 0; i < json.bus.length; i++) {
        var tempbus = new bus(i + 1, bustype[json.bus[i][1][1] - 1], json.bus[i][11][1], json.bus[i][12][1], json.bus[i][9][1]);

        for (var x = 0; x < featureNamesBus.length; x++) {
            var tempdata = [];
            for (var j = 0; j < json.bus[i][featureNamesBus[x].index].length; j++) {
                tempdata.push(json.bus[i][featureNamesBus[x].index][j]);
            }
            var tempfeature = new feature(featureNamesBus[x].name, featureNamesBus[x].description, tempdata);
            tempbus.features.push(tempfeature);
        }
        interfaceData.bus.push(tempbus);
    };

    //fill in the branch data
    for (var i = 0; i < json.branch.length; i++) {
        var tempbranch = new branch(json.branch[i][0][1], json.branch[i][1][1], json.branch[i][2][1], json.branch[i][3][1], json.branch[i][10][1], json.branch[i][5][1], json.branch[i][6][1], json.branch[i][7][1]);

        for (var x = 0; x < featureNamesBranch.length; x++) {
            var tempdata = [];
            for (var j = 0; j < json.branch[i][featureNamesBranch[x].index].length; j++) {
                tempdata.push(json.branch[i][featureNamesBranch[x].index][j]);
            }
            var tempfeature = new feature(featureNamesBranch[x].name, featureNamesBranch[x].description, tempdata);
            tempbranch.features.push(tempfeature);
        }
        interfaceData.branch.push(tempbranch);
    }

    //fill in the generator data
    for (var i = 0; i < json.gen.length; i++) {
        var tempgen = new gen(json.gen[i][0][1], json.gen[i][3][1], json.gen[i][4][1], json.gen[i][7][1]);

        for (var x = 0; x < featureNamesGen.length; x++) {
            var tempdata = [];
            for (var j = 0; j < json.gen[i][featureNamesGen[x].index].length; j++) {
                tempdata.push(json.gen[i][featureNamesGen[x].index][j]);
            }
            var tempfeature = new feature(featureNamesGen[x].name, featureNamesGen[x].description, tempdata);
            tempgen.features.push(tempfeature);
        }
        interfaceData.gen.push(tempgen);
    }
    //Load presets
    //preset 1 active power generation/active power demand/ line currents
    presets[0] = {
        bus: [{
            name: featureNamesBus[0].name,
            index: 0,
            description: featureNamesBus[0].description
        }],
        gen: [{
            name: featureNamesGen[0].name,
            index: 0,
            description: featureNamesGen[0].description
        }],
        branch: [{
            name: featureNamesBranch[2].name,
            index: 2,
            description: featureNamesBranch[2].description
        }]
    };
    //preset 2 reactive power generation/reactive power demand/ voltages
    presets[1] = {
        bus: [{
            name: featureNamesBus[1].name,
            index: 1,
            description: featureNamesBus[1].description
        }, {
            name: featureNamesBus[2].name,
            index: 2,
            description: featureNamesBus[2].description
        }],
        gen: [{
            name: featureNamesGen[1].name,
            index: 1,
            description: featureNamesGen[1].description
        }],
        branch: []
    };

    callback();
};
