// Interface for the internal data processing

//general interface consisting of three arrays: bus, gen and branch. Each array will contain multiple bus-objects for the bus array, branch objects for the branch array and gen objects for the gen array.
function dataInterface() {
    this.bus= new Array();
    this.gen= new Array();
    this.branch= new Array();
}

//A bus object contains one array "features" which contains multiple feature objects and four objects for the name, type vmin and vmax.
function bus(name,type, vmax, vmin, baseKV){
    this.features=new Array();
    this.name=name;
    this.bustype=type;
    this.vmax=vmax;
    this.vmin=vmin;
    this.baseKV=baseKV;
}

//A branch object contains one array "features" which contains multiple feature objects and five objects. "from" and "to" describes on which busses the branch is fixed. status is either 0 or 1 for out-of-service and in-service respectively
function branch(from, to, resistance, reactance, status, rateA, rateB, rateC){
    this.from=from;
    this.to=to;
    this.rateA=rateA;
    this.rateB=rateB;
    this.rateC=rateC;
    this.resistance=resistance;
    this.reactance=reactance;
    this.status=status;
    this.features=new Array();
}

//A gen(short for generator) object contains one array "features" which contains multiple feature objects and four objects. genBus describes on which bus the generator is. genstatus is either less or equal to 0 or bigger than 0 for machine out-of-service and machine in-service respectively
function gen(genBus, qmax, qmin, genStatus){
    this.genBus=genBus;
    this.qmax=qmax;
    this.qmin=qmin;
    this.genStatus=genStatus;
    this.features=new Array();
}

//A feature object holds the name and description of the feature and an array with its data over all timesteps.
function feature(name, description, data){
    this.name=name;
    this.description=description;
    this.data=data;
}

//General global variables
//the object for the timeslider
var slider;
//the state of the playbutton
var state = 'stop';
//the time for the timeslider to complete one loop
var timeToComplete= 1200;
//struct for the selected Elements and the selected features and the tabId which the features are on
var selected = {
    features:new Array(2),
    elements:{
        bus:[],
        gen: [],
        branch:[]
    }
};
//place the struct for the tab 1 which is always existing
selected.features[1]={
        tabId:"#Tab1",
        bus:[],
        gen: [],
        branch:[]
    };
// active tab
var activeTab=1;
//Presets to automatically show graphs of
// - active power generation / active power demand / line currents
// - reactive power generation/ demand/ voltages
var presets=[];
//array of charts such that one can access the chartobject for axample to update it
var charts= new Array(2);
//create the first empty chartobject for the first tab
charts[1]={
    busCharts:[],
    genCharts:[],
    branchCharts:[]
};
// current time for slider and graph
var currenttime=0;

//d3 scale from the timegraph
var timeGraphScale;

// object and constructor for the chartdata
var chartdata=function(name, xName, yName, width, height) {
    this.graphdata= new Array();
    this.name= name;
    this.xName= xName;
    this.yName= yName;
    this.width= width;
    this.height=height;
    this.convertToGraphdata= function(rawdata, nodes, featureIndex, objectName){
        var tempgraphdata=[];
        for (var z=0; z < nodes.length; z++){
            var tempdata = [];
            var index
            //the index of the data is not the same as the element number. Therefore we decrase it.
            index = nodes[z]-1;
            //numberOftimeSteps sets the maximum of timesteps
            for (var j=0; j<numberOfTimesteps; j++){
                    tempdata.push({x: j, y: rawdata[index].features[featureIndex].data[j]});
                }
            this.graphdata.push({ 
                        
                        values: tempdata,
                        key: objectName + " " + nodes[z]
            });
        }
    };
    
};