var numberOfTabs = 1; 

// JavaScript Document for the timegraph. It handles the plot and update of the graph and creates the accordion and selectables

// create the selectables on the sidebar to select the features
function createSelectable(data) {
    "use strict";
    for (var j=0; j< data.bus[0].features.length; j++){
        $("#busSelectOptions").append("<option data-index='"+ j + "' data-name='"+ data.bus[0].features[j].name + "'>"+ data.bus[0].features[j].description + "</option>");
    }
    for (var j=0; j< data.gen[0].features.length; j++){
        $("#genSelectOptions").append("<option data-index='"+ j + "' data-name='"+ data.gen[0].features[j].name + "'>"+ data.gen[0].features[j].description + "</option>");
    }
    for (var j=0; j< data.branch[0].features.length; j++){
        $("#branchSelectOptions").append("<option data-index='"+ j + "' data-name='"+ data.branch[0].features[j].name + "'>"+ data.branch[0].features[j].description + "</option>");
    }
    
    $('#busSelect').on('hidden.bs.collapse', function (e) {
        // Action to execute once the collapsible area is hidden
        selected.features[activeTab].bus=[];
        $(this).find("option").each(function(){
            var name = $(this).data('name');
            var index=$(this).data('index');
            var description =$(this).html();
            if(this.selected){selected.features[activeTab].bus.push({name: name, index: index, description:description});}
        });
        updateCharts();
    });
    $('#genSelect').on('hidden.bs.collapse', function (e) {
        // Action to execute once the collapsible area is hidden
        selected.features[activeTab].gen=[];
        $(this).find("option").each(function(){
            var name = $(this).data('name');
            var index=$(this).data('index');
            var description =$(this).html();
            if(this.selected){selected.features[activeTab].gen.push({name: name, index: index, description:description});}
        });
        updateCharts();
    });
    $('#branchSelect').on('hidden.bs.collapse', function (e) {
        // Action to execute once the collapsible area is hidden
        selected.features[activeTab].branch=[];
        $(this).find("option").each(function(){
            var name = $(this).data('name');
            var index=$(this).data('index');
            var description =$(this).html();
            if(this.selected){selected.features[activeTab].branch.push({name: name, index: index, description:description});}
        });
        updateCharts();
    });
    
}
//hide the timegraph div by default
$('#sidebar-right').hide();

//show/hide the timegraph div and update the charts
function showtimegraph(){
    if (!($('#sidebar-right').is(":visible"))) {
        $('#legendButton').text("Open Legend");
        $('#graph').animate({width: "50%"}, 300, function(){
            centerGroup(); 
        });
        $('#sidebar-right').animate({width: "50%"}, 300).show('fast', function(){ 
            updateCharts();
        });
        
		$('#timegraphbutton').text("Close Timegraph");
        
    }
    else {
        $('#sidebar-right').animate({width: "0%"}, 300).hide();
        $('#legendButton').text("Close Legend");
		$('#graph').animate({width: "100%"}, 300, function(){
            centerGroup(); 
            drawLegend();
        });
		$('#timegraphbutton').text("Open Timegraph");
		
    }
}
//Function to load the presets inso the struct "selected"
function loadPreset(presetNumber){
    selected.features[activeTab].bus=presets[presetNumber].bus;
    selected.features[activeTab].gen=presets[presetNumber].gen;
    selected.features[activeTab].branch=presets[presetNumber].branch;
    updateCharts();
}
//make the tabs dynamicall by letting the user create and remove tabs
$(".nav-tabs")
    .on("click", "input", function (e) {
        e.preventDefault();
        $(this).parent().tab('show');
        var activeId=$(this).parent().attr('href');
        activeTab=parseInt(activeId.substr(activeId.length-1));
        
        updateCharts();
        
    })
    .on("click", "span", function (e) {
        e.preventDefault();
        //if the closed tab is the current tab switch to tab 1
        if($(this).parent().attr('href')=='#Tab'+activeTab){
            activeTab=1;
            updateCharts();
            $("#Tab1").addClass("show").addClass("active");
            $('.nav-tabs a[href="#Tab1"]').addClass("active").addClass("show");
        }
        //remove the old tabs
        $(this).parent().parent().remove();
        $($(this).parent().attr('href')).remove();
    });
//add a tab
$('.add-tab').click(function (e) {
    
    e.preventDefault();
    
    
    numberOfTabs =numberOfTabs+1;
    var tabName = 'Tab ' + numberOfTabs;
    var tabId = 'Tab'+numberOfTabs;
    selected.features.push({
        tabId:"#"+tabId,
        bus:[],
        gen: [],
        branch:[]
    });
    charts.push({
    busCharts:[],
    genCharts:[],
    branchCharts:[]
    });
    $(this).closest('li').before('<li class="nav-item"><a class="nav-link d-inline-block" href="#'+tabId+'" role="tab" data-toggle="tab"><input style="cursor:pointer" class="form-control-plaintext d-inline-block w-75" type="text" placeholder="'+tabName+'" value="'+tabName+'"readonly="true" ondblclick="this.readOnly=\'\';" onblur="this.readOnly=\'true\';"><span class="d-inline-block" style="float:right"> &times; </span> </a> </li>');
    
    $('.tab-content').append('<div role ="tabpanel" class="tab-pane fade" id="' + tabId + '"><div class="busCharts"></div><div class="genCharts"></div><div class="branchCharts"></div></div>');
    activeTab=numberOfTabs; 
    $('.nav-tabs a[href="#'+tabId+'"]').tab('show');
    
});
//function to update one sort of elements, for example busses
function updateElement(div, name, selectedFeatures, selectedElements, chartObjects, ifdata){
    var height=$("#sidebar-right").height()/4;
    var width=$("#sidebar-right").width();
    var chartId;
    if(selectedFeatures.length == 0){$(div).hide();}
    
    for (var i=0; i<selectedFeatures.length; i++){
        $(div).show();
        var chartExists= false;
        var chartNumber;
        for (var j=0; j<chartObjects.length; j++ ){
            if(chartObjects[j].name === selectedFeatures[i].name){
                chartExists=true;
                chartNumber=j;
                break;
            }
        }
        if(chartExists){
            //Update and show chart if it exists
            
            $(chartObjects[chartNumber].div).show();
            var newChartData= new chartdata(selectedFeatures[i].description, "Time", selectedFeatures[i].name, $(chartObjects[chartNumber].div).width(), height);
            newChartData.convertToGraphdata(ifdata, selectedElements, selectedFeatures[i].index, name);
            chartObjects[chartNumber].chartSvg.datum(newChartData.graphdata).transition().duration(500).call(chartObjects[chartNumber].chartObject);
            nv.utils.windowResize(chartObjects[chartNumber].chartObject.update);
            if ( $( ".nv-interactive line" ).length ) {
 
                $( ".nv-interactive line" ).attr("y2", $(chartObjects[chartNumber].div + ' svg').find('rect').height()).attr('transform', 'translate('+timeGraphScale(currenttime)+',0)');
 
            }
            else{
                d3.selectAll(".nv-line")
                .append("line")
                    .style("stroke", "red")
                    .style("stroke-width", 1)
                    .attr("x1", 0)
                    .attr("y1", 0)
                    .attr("x2", 0)
                    .attr("y2", $(chartObjects[chartNumber].div + ' svg').find('rect').height()).attr('transform', 'translate('+timeGraphScale(currenttime)+',0)');
            }
        }
        else{
            //plot the new chart
            
            var newChartObj;
            var newChartSvg;
            var newChart={
                name:selectedFeatures[i].name,
                div: "unset",
                chartObject: newChartObj,
                chartSvg: newChartSvg
            };
            chartNumber= chartObjects.push(newChart)-1;
            var elementName=name;
            chartId= elementName + chartNumber;
            chartObjects[chartNumber].div=div+" ."+chartId;
            $(div).append('<div class="'+chartId+'"></div>');
            var newChartData= new chartdata(selectedFeatures[i].description, "Time", selectedFeatures[i].name, width, height);
            newChartData.convertToGraphdata(ifdata, selectedElements, selectedFeatures[i].index, name);
            $(chartObjects[chartNumber].div).append('<div style="display:inline"><strong>'+newChartData.name+'</strong><button style="float:right" class="btn" onClick="pngExport(this,\''+elementName+'-'+ newChartData.name +'\')"><i class="fas fa-download"></i></button><div>');
            plotLineChart(chartObjects[chartNumber], newChartData);
            
        }
    }
    
    // hide charts that are not used anymore
    for (var i=0; i<chartObjects.length;i++){
        var chartHide= true;
        var chartNumber;
        for (var j=0; j<selectedFeatures.length; j++ ){
            if(chartObjects[i].name==selectedFeatures[j].name){
                chartHide=false;
                chartNumber=j;
                break;
            }
        }
        if(chartHide){
            //hide unused charts
            $(chartObjects[i].div).hide();
        }
    }
}
//function to plot one single linechart
function plotLineChart(chart, chartData){
    nv.addGraph(function() {
                chart.chartObject = nv.models.lineChart().options({useInteractiveGuideline: true, width: chartData.width, height: chartData.height});
                // chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
                var timeIncrement= 1440/numberOfTimesteps;
                var zero = d3.format("02d");
                var tickFormatter = function(d){
                    var rawTime = 0+timeIncrement*d;
                    return zero(Math.floor(rawTime/60))+":"+zero(d3.round(rawTime%60));
                };
                chart.chartObject.xAxis
                    .axisLabel(chartData.xName)
                    .tickFormat(tickFormatter);

                chart.chartObject.yAxis
                    .axisLabel(chartData.yName)
                    .tickFormat(function(d) {
                        if (d == null) {
                            return 'N/A';
                        }
                        return d3.format(',.2f')(d);
                    });
                d3.select(chart.div).append('svg')
                    .attr('viewBox', '0 0 ' + chartData.width + ' ' + chartData.height);
                chart.chartSvg = d3.select(chart.div+' svg').datum(chartData.graphdata);
                chart.chartSvg.transition().duration(500).call(chart.chartObject);

                //get the scale of the chart
                timeGraphScale=chart.chartObject.xScale();

                //move the timeslider to the clicked timep in the graph
                chart.chartObject.lines.dispatch.on('elementClick', function(e) {
                        currenttime=e[0].pointIndex;
                        d3.selectAll('.nv-line').selectAll('line').attr('transform', 'translate('+timeGraphScale(currenttime)+',0)');
                        slider.setValue(currenttime);    
                    });
                //add a windowResize function such that the graph changes its size when the window is resized
                nv.utils.windowResize(function() {
                    chart.chartObject.update;
                      });

                return chart;
              },function(){d3.selectAll(".nv-interactive")
                    .append("line")
                    .style("stroke", "red")
                    .style("stroke-width", 1)
                    .attr("x1", 0)
                    .attr("y1", 0)
                    .attr("x2", 0)
                    .attr("y2", $(chart.div + ' svg').find('rect').height()).attr('transform', 'translate('+timeGraphScale(currenttime)+',0)');
                          });
}
//function to update all charts
async function updateCharts(){
    
    
    //Update Bus
    updateElement(selected.features[activeTab].tabId+" .busCharts", "Bus",selected.features[activeTab].bus, selected.elements.bus, charts[activeTab].busCharts, interfaceData.bus);
    //Update Generators
    updateElement(selected.features[activeTab].tabId+" .genCharts", "Generator",selected.features[activeTab].gen, selected.elements.gen, charts[activeTab].genCharts, interfaceData.gen);
    //Update Branches
    updateElement(selected.features[activeTab].tabId+" .branchCharts", "Branch",selected.features[activeTab].branch, selected.elements.branch, charts[activeTab].branchCharts, interfaceData.branch);
}