//Export the graph svg as png
d3.select("#saveButton").on('click', function(){

    // Get the d3js SVG element and save using saveSvgAsPng.js
    saveSvgAsPng(document.getElementsByTagName("svg")[0], "networkGraph.png", {scale: 2, backgroundColor: "#FFFFFF"});
    
});

async function pngExport(element, name){
    
    var svg= $(element).parent().parent().find('svg')[0];
    $(".nv-interactive").hide();
    await saveSvgAsPng(svg, name+".png", {scale: 2, backgroundColor: "#FFFFFF"});
    $(".nv-interactive").show();
    
}