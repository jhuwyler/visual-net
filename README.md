# Visualization of Power Grids

This web-based tool is to visualize Power Grid data in the MATPOWER format. It combines a grid topology view with time charts. There are built in examples to explore the tool. It was inspired by the Steady-State AC Network Visualization in the browser by the Monash university (http://immersive.erc.monash.edu.au/stac/)

There is the option to import your own data by writing a data specific import function.

This project was part of a group project at the PSL at the ETH Zurich and was created in the Spring Semester 2018.

The whole program is documented in a pdf file.

## Getting Started

Fork the project or download it. Open the index.html file or open the matlab script to start the tool from MATLAB. 

### Prerequisites

There are no further prerequisites to run this. If you want to export PNG pictures it is recommended running the software on a server, since the saving of a PNG does not work properly offline.


## Built With

* [D3](http://www.d3js.org/) - For displaying data
* [JQUERY](https://jquery.com/) - For coding efficently
* [BOOTSTRAP](https://getbootstrap.com) - For a fluent layout
* [D3.SLIDER](http://sujeetsr.github.io/d3.slider/) - For the timeslider
* [NVD3](http://nvd3.org/) - For displaying time charts
* [WEBCOLA](http://ialab.it.monash.edu/webcola/) - For constrained based layout of the network
* [D3-TOOLTIP](http://labratrevenge.com/d3-tip/) - For an interactive tooltip
* [SAVEASPNG](https://github.com/exupero/saveSvgAsPng) - For saving the network or charts as a PNG picture

## Authors

* **Lioba Heimbach** - *Visualization of grid data*
* **Dario Küffer** - *Network topology*
* **Julian Huwyler** - *Time charts and sidebar*
